# solarsim

### 1.0:
N-Body Solar System Simulation with point mass insertion. Solarsim takes advantage of the predictor-corrector algorithm to solve the 2nd Order system of differential equations describing the motion of the planets and point mass within the system. The acceleration term is used as the corrector. Coefficients are taken from:

    Gear, C. W., "The Numerical Integration of Ordinary Differential Equations of Various Orders," Argonne National Laboratory, Technical Report 7126, Argonne, Illinois, January 1966.

### Upcoming Changes
- Timestep defined ΔV for the Point Mass
- Define Point Mass position and velocity relative to a planet's center
- Define Point Mass orbit relative to a planet with Keplerian OEs
- Define Custom Planet orbits relative to the central star with Keplerian OEs
- Pre-defined Custom Planets based on the dwarf planets in the Solar System

----

## Requirements
- autopep8 1.6.0
- coverage 6.3.2
- pylint 2.13.7
- datetime 4.4
- requests 2.27.1
- numpy 1.22.3
- matplotlib 3.5.1

----

## Screenshots
![solarsim main GUI](https://i.imgur.com/v6WQhEH.png)
<br>Main GUI

<br>

![solarsim graph](https://i.imgur.com/YD1Uj34.png)
<br>Results Graph
