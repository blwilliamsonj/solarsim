# ----------------------------------------------------------------------------
# Bryan Williamson
# 
# --|Orbital.py|--------------------------------------------------------------
# Orbital.py contains various methods to assist with orbital mechanics
# calculations. Methods listed below.
# 
# ----|Methods|----
# rot1 -  Create a 3x3 numpy array containing the 1 rotation matrix of the
#         input angle.
# rot2 -  Create a 3x3 numpy array containing the 2 rotation matrix of the
#         input angle.
# rot3 -  Create a 3x3 numpy array containing the 3 rotation matrix of the
#         input angle.
# rv2oe - Convert radius and velocity to orbital elements
# 
# oe2rv - Convert orbital elements to radius and velocity
# 
# coordChange - Convert a 3D vector from one coordinate system to another
# ----------------------------------------------------------------------------

# --|Imports|-----------------------------------------------------------------
import numpy as np
import spaceObj as so


# --|GLOBALS|-----------------------------------------------------------------
IHAT = np.array([1,0,0], 'float64')
JHAT = np.array([0,1,0], 'float64')
KHAT = np.array([0,0,1], 'float64')
G = 6.6743E-1 # (m^3)/(kg*s^2) or (N*m^2)/(kg^2)


# --|Methods|-----------------------------------------------------------------
def rot1(a):
    """
    rot1(a)
    
    Return the 1 rotation matrix about the input angle.

    INPUT:
    a : float
        Angle about which the rotation matrix is created.
        Angle defined in radians

    OUTPUT:
    rot : numpy array
          3x3 numpy array containing the 1 rotation matrix about angle a.
    """
    
    rot = np.array([[1.0,          0.0,       0.0],
                    [0.0,    np.cos(a), np.sin(a)],
                    [0.0, -1*np.sin(a), np.cos(a)]])
    return rot

def rot2(a):
    """
    rot2(a)
    
    Return the 2 rotation matrix about the input angle.

    INPUT:
    a : float
        Angle about which the rotation matrix is created.
        Angle defined in radians

    OUTPUT:
    rot : numpy array
          3x3 numpy array containing the 2 rotation matrix about angle a.
    """
    
    rot = np.array([[np.cos(a), 0.0, -1*np.sin(a)],
                    [      0.0, 1.0,          0.0],
                    [np.sin(a), 0.0,    np.cos(a)]])
    return rot


def rot3(a):
    """
    rot3(a)
    
    Return the 2 rotation matrix about the input angle.

    INPUT:
    a : float
        Angle about which the rotation matrix is created.
        Angle defined in radians

    OUTPUT:
    rot : numpy array
          3x3 numpy array containing the 2 rotation matrix about angle a.
    """
    
    rot = np.array([[   np.cos(a), np.sin(a), 0.0],
                    [-1*np.sin(a), np.cos(a), 0.0],
                    [         0.0,       0.0, 1.0]])
    return rot


def rv2oe(central, satellite):
    """
    rv2oe(central, satellite)
    
    Convert a radius and velocity vector to Keplerian Orbital Elements.

    INPUT:
    central : SpaceObj object
        SpaceObj object (Star or Planet) that the satellite is orbiting.
    satellite : SpaceObj object
        SpaceObj object (Planet or PointMass) defining the satellite. Radius
        and velocity vectors are taken from the satellite's properties

    OUTPUT:
    oe : numpy array
         6 element column vector containing the classical Keplerian Orbital
         Elements.
         Units are kilograms, meters, seconds, and radians
         Elements are in the following order:
             [p; a; e; i; Omega; omega; nu]
    """
    
    # --|Useful Variables|----------------------------------------------------
    r, v = satellite.radius, satellite.velocity
    rMag, vMag = np.linalg.norm(r), np.linalg.norm(v)
    mu = G*(central.mass + satellite.mass)
    
    # --|Calculate OE's|------------------------------------------------------
    # Angular Momentum
    h = np.cross(r,v)
    hMag = np.linalg.norm(h)
    
    # Ascending Node
    nAN = np.cross(KHAT,h)
    nANMag = np.linalg.norm(nAN)
    
    # ----|Eccentricity|----
    e = ((vMag**2 - mu/rMag)*r - np.dot(r,v)*v)/mu
    eMag = np.linalg.norm(e)
    
    # ----|Semimajor Axis and Semiparameter|----
    xi = 0.5*vMag**2 - mu/rMag
    
    if eMag != 1.0:
        a = -1*mu/(2*xi)
        p = a*(1 - eMag**2)
    else:
        a = np.inf
        p = (h**2)/mu
    
    # ----|Inclination, RAAN, Argument of Perigee, and True Anomaly|----
    i = np.arccos(h[2]/hMag)
    
    Omega = np.arccos(nAN[0])
    if nAN[1] < 0: Omega = 2*np.pi - Omega
    omega = np.arccos(np.dot(nAN,e)/(nANMag*eMag))
    nu = np.arccos(np.dot(e,r)/(eMag*rMag))
    
    oe = np.array([p,a,e,i,Omega,omega,nu])
    return oe


def oe2rv(oe, central, satellite):
    """
    oe2rv(oe, central, satellite)
    
    Convert the orbital elements of satellite into its radius and velocity
    vectors about it's central body.
    Units are meters, and meters per second
    
    INPUT:
    oe : numpy array
        7 element vector containing the keplerian orbital elements for the
        satellite object.
             [p; a; e; i; Omega; omega; nu]
    central : SpaceObj object
        SpaceObj object (Planet or Star) about which satellite orbits.
    satellite : SpaceObj object
        SpaceObj object (Planet or PointMass) that is orbiting central.

    OUTPUT:
    rv : numpy array
        2x3 numpy array containing the radius vector in the first row and the
        velocity vector in the second row.
    """
    
    # --|Useful Variables|----------------------------------------------------
    mu = G*(central.mass + satellite.mass)
    
    # --|Calculate Radius and Velocity Vectors|-------------------------------
    eMag = np.linalg.norm(oe[2])
    r = np.array([(oe[0]*np.cos(oe[6]))/(1 + eMag*np.cos(oe[6])),
                  (oe[0]*np.sin(oe[6]))/(1 + eMag*np.cos(oe[6])),
                  0.0])
    v = np.array([-1*np.sin(oe[6])*(mu/oe[0])**0.5,
                  (eMag + np.cos(oe[6]))*(mu/oe[0])**0.5,
                  0.0])
    
    # Convert to IJK
    toIJK = rot3(-1*oe[4])*rot1(-1*oe[3])*rot3(-1*oe[5])
    
    r *= toIJK
    v *= toIJK
    
    rv = np.concatenate([r,v])
    rv.shape = [2,3]
    
    return rv
























