# ----------------------------------------------------------------------------
# Bryan Williamson
# 
# --|getEph.py|---------------------------------------------------------------
# Using the internet API interface to the JPL Horizons system, request and
# save a combined ephemeris for all user requested solar system objects.
# Combined ephemera txt file will be saved in the /eph/ subdirectory of the
# main project directory.
# ----------------------------------------------------------------------------

# --|Imports|-----------------------------------------------------------------
import json
import requests
import numpy as np
from datetime import datetime, timedelta
from os.path import dirname, realpath

# --|GLOBALS|-----------------------------------------------------------------
EPHDIR = "/".join([dirname(dirname(realpath(__file__))), "eph/"])
ID = {"Sun": 10, "Mercury": 199, "Venus": 299, "Earth": 399, "Mars": 499,
      "Jupiter": 599, "Saturn": 699, "Uranus": 799, "Neptune": 899,
      "Pluto": 999}


# --|Methods|-----------------------------------------------------------------
def oneMinIncr(time):
    """
    oneMinIncr(time)
    
    Increment the input time by one minute. For use in grabbing the ephemeris
    files.

    INPUT:
    time : string
        Time string in the following format:
            YYYY-MMM-DD hh:mm:ss

    OUTPUT:
    new : string
        Time string in the same format as the input time. new is exactly one
        minute after time.
    """
    
    # Create datetime object
    new = datetime.strptime(time, "%Y-%b-%d %H:%M:%S")
    
    # Increment time
    new += timedelta(minutes = 1)
    
    # Convert back to string
    new = new.strftime("%Y-%b-%d %H:%M:%S")
    
    return new


def getEph(bodies, start, fileName, reports = False):
    """
    getEph(bodies, start, fileName)
    
    getEph connects to the JPL Horizons System to grab the requested
    ephemerides, save them to a file, and return the file's full location and
    name.

    INPUT:
    bodies : list
        List of known planet name strings that ephemerides will be
        requested for.
    start : string
        Time string specifying the initial time for the ephemeris requests.
        start is in the following format:
            YYYY-MMM-DD hh:mm:ss
    fileName : string
        String specifying the name of the ephemerides file.
        fileName is in the following format:
            yourFileName.txt

    OUTPUT:
        .txt file containing the requested planet(s) position and
        velocity data
    """
    
    # ----|Define Times|----
    stop = oneMinIncr(start)
    
    # Add AD to enable dates before 2000
    start = " ".join(["AD", start])
    stop = " ".join(["AD", stop])
    
    # Define list of body SPK IDs to get ephemerides for
    bodyIDs = [ID[body] for body in bodies]
    numBodies = len(bodyIDs)
    
    # Build appropriate URL for the request
    # Define API URL
    url = 'https://ssd.jpl.nasa.gov/api/horizons.api'
    url += "?format=json&EPHEM_TYPE=VECTORS&OBJ_DATA=YES&CENTER=@sun"
    url += "&TIME_DIGITS=SECONDS"
    url += "&COMMAND='{}'&START_TIME='{}'&STOP_TIME='{}'"
    
    # Get ephemerides    
    fileSpec = "".join([EPHDIR, fileName])
        
    with open(fileSpec, "w") as ephFile:
        written = 0
        
        for i in range(numBodies):
            # Make Horizons request
            response = requests.get(url.format(bodyIDs[i], start, stop))
            
            try:
                data = json.loads(response.text)
            except ValueError:
                if reports:
                    print("Unable to decode JSON results")
            
            # Check for valid request
            if response.status_code == 200:
                if "result" in data:
                    # Write body name to file
                    ephFile.write(bodies[i])
                    ephFile.write("\n")
                    
                    # Extract vectors only
                    data = data["result"].split("\n")
                    data = data[data.index("$$SOE") + 1: data.index("$$EOE")]
                    data = "\n".join(data)
                    
                    # Write body data to file
                    ephFile.write(data)
                    ephFile.write("\n")
                    
                    written += 1
            
            elif reports:
                print("ERROR: Ephemeris file not generated")
                print("RESPONSE CODE:", response.status_code)
                if "message" in data:
                    print("MESSAGE:", data["message"])
                else:
                    print(json.dumps(data, indent = 2))
        if reports:
            if written > 0:
                print("Successfully wrote {0} of {1}".format(written,
                                                             numBodies),
                      ["ephemeris", "ephemerides"][numBodies > 1])
            else:
                print("Failed to write data to file!")


def createEph(fileName):
    """
    createEph(fileName)
    
    Retrieve initial position and velocity data stored in the text file
    specified by fileName and convert the data to 2D dictionary.

    INPUT:
    fileName : string
        String containing the file name of the file containing the raw text
        position and velocity data for each body of interest from the JPL
        Horizons database.

    OUTPUT:
    ephemeris : dictionary
        2D dictionary containing the body names as the keys for the top layer
        of the dictionary. Each body entry has the center position and
        velocity as keys, and numpy vectors as values.
    """
    
    with open("".join([EPHDIR, fileName]), "r") as ephFile:
        data = ephFile.readlines()
    
    # Create 2D Dict
    numBodies = int(len(data)/5)
    ephemeris = dict()
    
    for body in range(numBodies):
        bodyName = data[body*5].strip()
        pos = data[body*5 + 2]
        vel = data[body*5 + 3]
        
        ephemeris[bodyName] = dict()
        ephemeris[bodyName]["center"] = np.array([float(pos[4:26]),
                                                  float(pos[30:52]),
                                                  float(pos[56:78])])*1E3
        ephemeris[bodyName]["velocity"] = np.array([float(vel[4:26]),
                                                    float(vel[30:52]),
                                                    float(vel[56:78])])*1E3
    
    return ephemeris