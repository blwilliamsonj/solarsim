# ----------------------------------------------------------------------------
# Bryan Williamson
# 
# --|prop.py|-----------------------------------------------------------------
# prop.py handles propogation of the N-Body simulation in the solarsim
# project. prop.py employs the predictor-corrector algorithm to calculate the
# N-body system. Coefficients for the Second-Order Predictor-Corrector Model
# are from:
#   Gear, C. W., "The Numerical Integration of Ordinary Differential Equations
#   of Various Orders," Argonne National Laboratory, Technical Report 7126,
#   Argonne, Illinois, January 1966.
# 
# ----|Methods|----
# getForcing - Get gravitational forcing between two bodies.
# 
# predict - Predict forward the gravitational forcing, position, velocity,
#   and acceleration of each of the bodies in the system.
# 
# correct - Correct the predicted values using the acceleration as the
#   correcting rate. The corrected values are corrected until the error in
#   position and velocity both are under the percentage tolerance. Once under
#   tolerance, the values are moved to the current values for the bodies in
#   the system, and the values are stored in the results dictionary for use
#   by the rest of the Solarsim program.
# 
# predCorr - Perform the first prediction to get the preidctor-corrector loop
#   going, step through each time step by calling predict, then correct.
# ----------------------------------------------------------------------------

# --|Imports|-----------------------------------------------------------------
import numpy as np


# --|GLOBALS|-----------------------------------------------------------------
# Orbital Mechanics
IHAT = np.array([1,0,0], 'float64')
JHAT = np.array([0,1,0], 'float64')
KHAT = np.array([0,0,1], 'float64')
G = 6.6743E-11 # (m^3)/(kg*s^2) or (N*m^2)/(kg^2)

# Predictor-Corrector Coefficients
C0, C1, C2, C3 = 1/6, 5/6, 1, 1/3


# --|Methods|-----------------------------------------------------------------
def getForcing(b1pos, b1mass, b2pos, b2mass):
    """
    getForcing calculates the gravitational force exerted on the first body
    by the second body, given their position vectors and masses. Units are
    meters, kilograms, and Newtons.

    INPUT:
    b1pos : numpy vector
        Vector representing the position of the first body.
    b1mass : float
        Mass of the first body.
    b2pos : numpy vector
        Vector representing the position of the second body.
    b2mass : TYPE
        DESCRIPTION.

    OUTPUT:
    force : Numpy Vector
        Gravitational force exerted on the first body by the second.
            (GMm)/r^2 -> Newton = (kg*m)/s^2
    """
    
    # Cubed magnitude of the distance between body1 and body2
    distCu = np.sqrt(np.sum((b1pos - b2pos)**2))**3
    
    # Distance vector from body1 to body2
    dist = b2pos - b1pos
    
    # Calculate force
    force = G*b1mass*b2mass*dist/distCu
    
    return force


def predict(star, bodies, timeStep):
    """
    predict handles the predictor phase of the 2nd-order predictor-corrector
    algorithm.

    INPUT:
    star : SpaceObj Star
        SpaceObj Star object used as the center of the simulation.
    bodies : list
        List of SpaceObj Objects that will be simulated.
    timeStep : float
        Number of seconds to pass in this timeStep.
    """
    
    # Predict forward each non star's position
    for body in bodies:
        body.rNext = (body.rCurr +
                      body.vCurr*timeStep +
                      body.aCurr*timeStep*timeStep*0.5 +
                      body.aPrime*timeStep*timeStep*timeStep/6)
    
    # Predict forward each body's Fg, velocity, and acceleration
    for body in bodies:
        # ----|Calc next Fg|----
        # Star contribution
        body.fNext = getForcing(body.rNext, body.mass, star.rCurr, star.mass)
        
        # All other bodies
        body.fNext += np.sum(np.array([getForcing(body.rNext, body.mass,
                                                  body2.rNext, body2.mass)
                                       for body2 in bodies
                                       if body.name != body2.name]),
                             axis = 0)
        
        # ----|Calc next v, a, and a'|----
        body.aNext = body.fNext/body.mass
        body.aPrime = (body.aNext - body.aCurr)/timeStep
        body.vNext = (body.vCurr +
                      body.aCurr*timeStep +
                      body.aPrime*timeStep*timeStep*0.5)
        
        body.aNext = body.aCurr + body.aPrime*timeStep


def correct(bodies, tol, index, results):
    """
    correct handles the corrector phase of the 2nd-order predictor-corrector
    algorithm.

    INPUT:
    bodies : list
        List of SpaceObj Objects that will be simulated.
    tol : float
        Float representing the percentage change tolerance for both the
        position and velocity of each body in the system.
    index : int
        Integer representing the point in time of the current step. Used in
        appending the new position to the results dictionary
    results : dictionary
        2D dictionary containing the positions at each time step for each
        body input by the user. Format is as follows:
            results = {"star": np.zeros(3),
                       "planets": {"planet1": np.array((numsSteps, 3)),
                                   "planet2": np.array((numsSteps, 3)),
                                       .
                                       .
                                       .
                                   "planetN": np.array((numsSteps, 3))},
                       "point": np.array((numsSteps, 3))}
    """
    
    # ----|Body Loop|----
    for body in bodies:
        # Reset errors
        rErr, vErr = 2*tol, 2*tol
        
        # Get initial corrector a
        aC = body.fNext/body.mass
        
        # ----|Corrector Loop|----
        while ((rErr > tol) and (vErr > tol)):
            deltaA = aC - body.aNext
            
            # Calculate corrector values
            rC = body.rNext + C0*deltaA
            vC = body.vNext + C1*deltaA
            aC = body.aNext + C2*deltaA
            body.aPrime = body.aPrime + C3*deltaA
            
            # Calculate Errors
            rErr = (np.sqrt(np.sum((body.rNext - rC)**2)) / 
                    np.sqrt(np.sum(body.rNext*body.rNext)))
            vErr = (np.sqrt(np.sum((body.vNext - vC)**2)) / 
                    np.sqrt(np.sum(body.vNext*body.vNext)))
            
            # Update values
            body.next2curr()
            body.rNext, body.vNext, body.aNext = rC, vC, aC
        
        # Errors within tolerance
        body.next2curr()
        
        # Append position to results
        if body.name[0:3] != "PM-":
            results["planets"][body.name][index] = body.rCurr
        else:
            results["point"][index] = body.rCurr


def predCorr(system, timeSteps, tol, results):
    """
    predCorr handles performing all predictor-corrector iterations on the
    system passed to it, and appending all body positions to the results
    dictionary.

    INPUT:
    system : dictionary
        Dictionary containing the star, planets, and point mass within the
        system. System takes on the the following format:
            system = {"star": spaceObj.Star Object,
                      "planets": [spaceObj.Planet Object 1,
                                  spaceObj.Planet Object 2,
                                      .
                                      .
                                      .
                                  spaceObj.Planet Object N],
                      "point": spaceObj.PointMass Object}
    timeSteps : list
        List of datetime objects containing the time at each simulation step.
    tol : float
        Float indicating the % of tolerance used in the corrector loop.
    results : dictionary
        2D dictionary containing the positions at each time step for each
        body input by the user. Format is as follows:
            results = {"star": np.zeros(3),
                       "planets": {"planet1": np.array((numsSteps, 3)),
                                   "planet2": np.array((numsSteps, 3)),
                                       .
                                       .
                                       .
                                   "planetN": np.array((numsSteps, 3))},
                       "point": np.array((numsSteps, 3))}
    """
    
    # ----|Determine System Type|----
    # Star, Planets, and a Point Mass
    if ("planets" in system.keys()) and ("point" in system.keys()):
        bodies = system["planets"] + [system["point"]]
    
    # Star and Planets
    elif "planets" in system.keys():
        bodies = system["planets"]
    
    # Star and Point Mass
    else:
        bodies = [system["point"]]
    
    # ----|First Step|----
    for body in bodies:
        # ----|Calculate Fg on Body|----
        # Star contribution
        body.fCurr = getForcing(body.rCurr, body.mass,
                                system["star"].rCurr, system["star"].mass)
        
        # Body contributions
        body.fCurr += np.sum(np.array([getForcing(body.rCurr, body.mass,
                                                  body2.rCurr, body2.mass)
                                       for body2 in bodies
                                       if body.name != body2.name]),
                             axis = 0)
        
        # ----|Calculate Current Acceleration|----
        body.aCurr = body.fCurr/body.mass
    
    # ----|Predictor-Corrector Loop|----
    # Get length of time step
    timeStep = timeSteps[1] - timeSteps[0] # timedelta object
    timeStep = timeStep.seconds + timeStep.microseconds/1E6 # float seconds
    
    # Loop, first time not needed as it is initial position
    for index, time in enumerate(timeSteps[1:]):
        # Predict
        predict(system["star"], bodies, timeStep)
        
        # Correct
        correct(bodies, tol, index + 1, results)