# ----------------------------------------------------------------------------
# Bryan Williamson
# 
# --|spaceObj.py|-------------------------------------------------------------
# spaceObj.py implements the Star, Planet, and Point classes for use in
# the main solarsim simulation.
# ----------------------------------------------------------------------------

# --|Imports|-----------------------------------------------------------------
import numpy as np


# --|SpaceObj Class|----------------------------------------------------------
class SpaceObj(object):
    """
    The SpaceObj class is the parent class to the Star, Planet, and PointMass
    classes. Center and velocity are defined in the HCI coordinate system.
    Units are meters, kilograms, and meters per second
    """
    
    def __init__(self, name, properties = {"mass": 0,
                                           "radius": 0,
                                           "center": np.zeros(3),
                                           "velocity": np.zeros(3)}):
        """
        Initialize a SpaceObj object for use in the simulated solar system.
        
        INPUT:
        name : string
            String defining the name of the object
        properties : dictionary
            Dictionary defining the mass, radius, position of the center in
            HCI, and velocity in HCI.
                mass : float
                radius : float
                center : numpy vector
                velocity : numpy vector
        """
        
        self.name = name
        self.mass = properties["mass"]
        self.radius = properties["radius"]
        self.rCurr = properties["center"]
        self.vCurr = properties["velocity"]
        
        # Initialize current a, current f, next r, next v, next a, aprime,
        # and fNext to [0]
        self.aCurr, self.fCurr = np.zeros(3), np.zeros(3)
        self.rNext, self.vNext = np.zeros(3), np.zeros(3)
        self.aNext, self.aPrime = np.zeros(3), np.zeros(3)
        self.fNext = np.zeros(3)
    
    
    def __str__(self):
        """
        Represent the object as a string.
        """
        
        return "".join([self.name, "\n\t",
                        "Type:     \n\t",
                        "Mass:     ", str(self.mass), "\n\t",
                        "Position: ", str(self.rCurr), "\n\t",
                        "Velocity: ", str(self.vCurr)])
    
    
    def __repr__(self):
        return self.__str__()
    
    
    def next2curr(self):
        """
        next2curr moves the values of rNext, vNext, aNext, and fNext to rCurr,
        vCurr, aCurr, and fCurr respectively.
        """
        
        self.rCurr, self.vCurr = self.rNext, self.vNext
        self.aCurr, self.fCurr = self.aNext, self.fNext


# --|Star Class|---------------------------------------------------------------
class Star(SpaceObj):
    """
    The Star class creates a Star object for use at the center of the
    simulated solar system. Center and velocity are defined in the HCI
    coordinate system.
    Units are meters, kilograms, and meters per second
    """
    
    # ----|CLASS VARIABLES|----
    # Sourced from:
    #    NASA Planetary Fact Sheet:
    #    https://nssdc.gsfc.nasa.gov/planetary/factsheet/
    PROPERTIES = {"Sun":{"mass": 1988500E24,
                         "radius": 695700E3,
                         "center": np.zeros(3),
                         "velocity": np.zeros(3)}}
    
    # ----|Methods|----
    def __init__(self, name, properties = {"center": np.zeros(3),
                                           "velocity": np.zeros(3)}):
        """
        Initialize a Star object according to either user defined values or
        according to predefined values. If the name is within the predefined
        star keys, the predefined star will be used. Stars always occur at the
        origin with 0 velocity.
        
        INPUT:
        name : string
            Name of the Star object. If this given name is within the
            dictionary of predefined stars, the predefined star will be used
            instead, and the passed properties, if any, will be ignored.
        properties : dictionary
            Dictionary defining the mass, radius, position of the center in
            HCI, and velocity in HCI.
                mass : float
                radius : float
                center : numpy vector
                velocity : numpy vector
        """
        
        # Check if predefined
        try:
            # Assemble properties
            properties["mass"] = self.PROPERTIES[name]["mass"]
            properties["radius"] = self.PROPERTIES[name]["radius"]
            
            # Create Star Object
            super().__init__(name = name, properties = properties)
            return
        except KeyError:
            # Custom Star
            properties["center"] = np.zeros(3)
            properties["velocity"] = np.zeros(3)
            super().__init__(name = name, properties = properties)
    
    
    def __str__(self):
        """
        Represent the object as a string.
        """
        
        string = super().__str__()
        index = string.index("Type:     ") + 10
        string = "".join([string[:index], "Star\n\t",
                          "Radius:   ", str(self.radius), "\n\t",
                          string[index:]])
        
        return string
    
    def __repr__(self):
        return self.__str__()
    
    
# --|Planet Class|------------------------------------------------------------
class Planet(SpaceObj):
    """
    The Planet class creates a Planet object for use in the
    solar system model. Center and velocity are defined in the HCI coordinate
    system.
    Units are meters, kilograms, and meters per second
    """
    
    # ----|CLASS VARIABLES|----
    # Sourced from:
    #    NASA JPL Horizons Planetary Physical Parameters
    #    https://ssd.jpl.nasa.gov/planets/phys_par.html
    PROPERTIES = {"Mercury": {"mass":   0.330103E24,
                              "radius": 2440.53E3},
                  "Venus":   {"mass":   4.86731E24,
                              "radius": 6051.8E3},
                  "Earth":   {"mass":   5.97217E24,
                              "radius": 6378.1366E3},
                  "Mars":    {"mass":   0.641691E24,
                              "radius": 3396.19E3},
                  "Jupiter": {"mass":   1898.1125E24,
                              "radius": 71492E3},
                  "Saturn":  {"mass":   568.317E24,
                              "radius": 60268E3},
                  "Uranus":  {"mass":   86.8099E24,
                              "radius": 25559E3},
                  "Neptune": {"mass":   102.4092E24,
                              "radius": 24764E3},
                  "Pluto":   {"mass":   13029E18,
                              "radius": 1188.3E3}}
    
    # ----|Methods|----
    def __init__(self, name, properties):
        """
        Initialize a Planet object according to either user defined values or
        according to predefined values. If the name passed is within the
        predefined planets, the predefined values will be used.
        
        INPUT:
        name : string
            Name of the Planet object. If this given name is within the
            dictionary of predefined planets, the predefined planet will be
            used instead, and the passed properties, if any, will be ignored.
        properties : dictionary
            Dictionary defining the mass, radius, position of the center in
            HCI, and velocity in HCI.
                mass : float
                radius : float
                center : numpy vector
                velocity : numpy vector
        """
        
        # Check if predefined
        try:
            # Assemble properties
            properties["mass"] = self.PROPERTIES[name]["mass"]
            properties["radius"] = self.PROPERTIES[name]["radius"]
            
            # Create Planet Object
            super().__init__(name = name, properties = properties)
            return
        except KeyError:
            # Custom Planet
            super().__init__(name = name, properties = properties)
    
    
    def __str__(self):
        """
        Represent the object as a string.
        """
        
        string = super().__str__()
        index = string.index("Type:     ") + 10
        string = "".join([string[:index], "Planet\n\t",
                          "Radius:   ", str(self.radius), "\n\t",
                          string[index:]])
        
        return string
    
    def __repr__(self):
        return self.__str__()


# --|PointMass Class|---------------------------------------------------------
class PointMass(SpaceObj):
    """
    The PointMass class creates a PointMass object for use in the in the
    solar system model. Center and velocity are defined in the HCI coordinate
    system.
    Units are meters, kilograms, and meters per second
    """
    
    # ----|Methods|----
    def __init__(self, name, properties):
        """
        Initialize a PointMass object. PointMass objects are a special case of
        SpaceObj in which the radius of the body is 0.
        
        INPUT:
        name : string
            Name of the Planet object. If this given name is within the
            dictionary of predefined planets, the predefined planet will be
            used instead, and the passed properties, if any, will be ignored.
        properties : dictionary
            Dictionary defining the mass, radius, position of the center in
            HCI, and velocity in HCI.
                mass : float
                radius : float
                center : numpy vector
                velocity : numpy vector
        """
        
        # Assemble properties
        properties["radius"] = 0
        
        # Create Object
        super().__init__(name = name, properties = properties)
    
    
    def __str__(self):
        """
        Represent the object as a string.
        """
        
        string = super().__str__()
        index = string.index("Type:     ") + 10
        string = "".join([string[:index], "Point Mass\n\t",
                          string[index:]])
        
        return string
    
    def __repr__(self):
        return self.__str__()
# ----------------------------------------------------------------------------