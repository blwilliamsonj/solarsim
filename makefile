.PHONY: solarsim.log

FILES :=                               \
    solarsim.html                      \
    solarsim.log                       \
    solarsim.py                        \
    run_solarsim.in                    \
    run_solarsim.out                   \
    run.py                             \
    test_solarsim.out                  \
    test_solarsim.py	               \
    lib/orbital.py                     \
    lib/prop.py                        \
    lib/spaceObj.py                    \
    lib/getEph.py                      \


ifeq ($(shell uname), Darwin)          # Apple
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := pydoc3
    AUTOPEP8 := autopep8
    DOC := docker run -it -v $$(PWD):/solarsim/ -w /solarsim/ blwilliamsonj/solarsim
else ifeq ($(shell uname -p), unknown) # Windows
    PYTHON   := python                 # on my machine it's python
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := python -m pydoc        # on my machine it's pydoc
    AUTOPEP8 := autopep8
    DOC := docker run -it -v /$$(PWD):/solarsim -w //solarsim/ blwilliamsonj/solarsim
else                                   # UTCS
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint3
    COVERAGE := coverage
    PYDOC    := pydoc3
    AUTOPEP8 := autopep8
    DOC := docker run -it -v $$(PWD):/solarsim/ -w /solarsim/ fareszf/python
endif

solarsim.html: solarsim.py
	$(PYDOC) -w solarsim

solarsim.log:
	git log > solarsim.log

run_solarsim.tmp: run_solarsim.in run_solarsim.out run.py solarsim.py
	$(PYTHON) run.py < run_solarsim.in > run_solarsim.tmp
	diff --strip-trailing-cr run_solarsim.tmp run_solarsim.out

test_solarsim.tmp: test_solarsim.py lib/spaceObj.py lib/getEph.py solarsim.py
	$(COVERAGE) run    --branch test_solarsim.py >  test_solarsim.tmp 2>&1
	$(COVERAGE) report -m                        >> test_solarsim.tmp
	cat test_solarsim.tmp > test_solarsim.out
	cat test_solarsim.out
	rm test_solarsim.tmp

check:
	@not_found=0;                                 \
    for i in $(FILES);                            \
    do                                            \
        if [ -e $$i ];                            \
        then                                      \
            echo "$$i found";                     \
        else                                      \
            echo "$$i NOT FOUND";                 \
            not_found=`expr "$$not_found" + "1"`; \
        fi                                        \
    done;                                         \
    if [ $$not_found -ne 0 ];                     \
    then                                          \
        echo "$$not_found failures";              \
        exit 1;                                   \
    fi;                                           \
    echo "success";

clean:
	rm -f  .coverage
	rm -f  *.pyc
	rm -f  run_solarsim.tmp
	rm -f  test_solarsim.tmp
	rm -rf __pycache__
	
docker:
	$(DOC)
	
config:
	git config -l

format:
	$(AUTOPEP8) -i solarsim.py
	$(AUTOPEP8) -i run.py
	$(AUTOPEP8) -i test_solarsim.py
	$(AUTOPEP8) -i lib/orbital.py
	$(AUTOPEP8) -i lib/prop.py

scrub:
	make clean
	rm -f  solarsim.html
	rm -f  solarsim.log

status:
	make clean
	@echo
	git branch
	git remote -v
	git status
	
versions:
	which       $(AUTOPEP8)
	$(AUTOPEP8) --version
	@echo
	which       $(COVERAGE)
	$(COVERAGE) --version
	@echo
	which       git
	git         --version
	@echo
	which       make
	make        --version
	@echo
	which       $(PIP)
	$(PIP)      --version
	@echo
	which       $(PYLINT)
	$(PYLINT)   --version
	@echo
	which        $(PYTHON)
	$(PYTHON)    --version

test: solarsim.html solarsim.log test_solarsim.tmp check
