# ----------------------------------------------------------------------------
# Bryan Williamson
# 
# --|test_solarsim.py|--------------------------------------------------------
# test_solarsim.py handles testing the methods and classes within the solarsim
# project to ensure proper functionality.
# ----------------------------------------------------------------------------

# --|Imports|-----------------------------------------------------------------
import os
from sys import path
from io import StringIO
from unittest import main, TestCase
from datetime import datetime
import numpy as np


# --|GLOBALS|-----------------------------------------------------------------
PROJDIR = os.path.dirname(os.path.realpath(__file__))
LIBDIR = PROJDIR + "/lib/"
EPHDIR = PROJDIR + "/eph/"
RESDIR = PROJDIR + "/res/"
path.insert(0, LIBDIR)
path.insert(0, EPHDIR)
CURTIME = datetime.now().strftime("%Y-%b-%d-%H-%M-%S")


# --|solarsim Imports|--------------------------------------------------------
import solarsim as ss
import getEph as eph
import spaceObj as so
import orbital as orb
import prop


# --|Tests|-------------------------------------------------------------------
class TestSolarsim(TestCase):
    # ----|GLOBALS|----
    global EPHDIR, LIBDIR, RESDIR, CURTIME
    
    
    # ----|CLASS VARIABLES|----
    START = "2001-Jan-01 12:00:00"
    SINGLEBODY = ["Uranus"]
    MULTIBODY = ["Mercury", "Venus", "Earth", "Mars",
                 "Jupiter", "Saturn", "Uranus", "Neptune"]
    SINGLEFILE = "".join([CURTIME, "-", str(len(SINGLEBODY)),
                          ["body", "bodies"][len(SINGLEBODY) > 1],
                          ".txt"])
    MULTIFILE = "".join([CURTIME, "-", str(len(MULTIBODY)),
                         ["body", "bodies"][len(MULTIBODY) > 1],
                         ".txt"])
    TOL = 0.001
    
    
    # --|spaceObj.py|---------------------------------------------------------
    # ----|Star Class|----
    def test_3_Star1(self):
        """
        Test predefined Star creation
        """
        
        Sun = so.Star(name = "Sun")
        
        self.assertEqual(Sun.name, "Sun")
        self.assertEqual(Sun.mass, 1988500E24)
        self.assertEqual(Sun.radius, 695700E3)
        
        test = np.zeros(3)
        
        for i in range(3):
            self.assertEqual(Sun.rCurr[i], test[i])
            self.assertEqual(Sun.vCurr[i], test[i])
    
    
    def test_3_Star2(self):
        """
        Test custom Star creation
        """
        
        star = so.Star("custom", {"mass": 10,
                                  "radius": 20})
        
        self.assertEqual(star.name, "custom")
        self.assertEqual(star.mass, 10)
        self.assertEqual(star.radius, 20)
        
        testCenter = np.zeros(3)
        testVelocity = np.zeros(3)
        
        for i in range(3):
            self.assertEqual(star.rCurr[i], testCenter[i])
            self.assertEqual(star.vCurr[i], testVelocity[i])
    
    
    def test_3_Planet1(self):
        """
        Test predefined Planet creation
        """
        
        # Get Positiion Data
        body = self.SINGLEBODY[0]
        ephemeris = eph.createEph(self.SINGLEFILE)
        properties = {"center": ephemeris[body]["center"],
                      "velocity": ephemeris[body]["velocity"]}
        
        # Create Planet
        Uranus = so.Planet(body, properties)
        
        self.assertEqual(Uranus.name, self.SINGLEBODY[0])
        self.assertEqual(Uranus.mass, 86.8099E24)
        self.assertEqual(Uranus.radius, 25559E3)
        
        # Check RV
        for i in range(3):
            # Check Center
            self.assertEqual(Uranus.rCurr[i],
                             ephemeris[body]["center"][i])
            
            # Check Velocity
            self.assertEqual(Uranus.vCurr[i],
                             ephemeris[body]["velocity"][i])
        
        # Remove SINGLEBODY file to prevent file build up
        fileSpec = "".join([EPHDIR, self.SINGLEFILE])
        if os.path.exists(fileSpec):
            os.remove(fileSpec)
    
    
    def test_3_Planet2(self):
        """
        Test custom planet creation
        """
        
        testCenter = np.array([1,2,3])
        testVelocity = np.array([4,5,6])
        planet = so.Planet("custom", {"mass": 10,
                                      "radius": 20,
                                      "center": testCenter,
                                      "velocity": testVelocity})
        
        
        
        self.assertEqual(planet.name, "custom")
        self.assertEqual(planet.mass, 10)
        
        for i in range(3):
            # Check Center
            self.assertEqual(planet.rCurr[i], testCenter[i])
            
            # Check Velocity
            self.assertEqual(planet.vCurr[i], testVelocity[i])
    
    
    def test_3_pointMass1(self):
        """
        Test point mass creation
        """
        
        properties = {"mass": 10,
                      "center": np.array([1,2,3]),
                      "velocity": np.array([4,5,6])}
        pointMass = so.PointMass("Test PointMass 1", properties)
        
        self.assertEqual(pointMass.name, "Test PointMass 1")
        self.assertEqual(pointMass.mass, properties["mass"])
        self.assertEqual(pointMass.radius, 0)
        for i in range(3):
            # Check Center
            self.assertEqual(pointMass.rCurr[i], properties["center"][i])
            
            # Check Velocity
            self.assertEqual(pointMass.vCurr[i], properties["velocity"][i])
        
    
    
    # --|getEph.py|-----------------------------------------------------------
    # ----|oneMinIncr()|----
    def test_1_oneMinIncr1(self):
        """
        Test leap day rollover
        """
        
        init = "2020-Feb-28 23:59:00"
        init = eph.oneMinIncr(init)
        
        test = "2020-Feb-29 00:00:00"
        
        self.assertEqual(init, test)
    
    
    def test_1_oneMinIncr2(self):
        """
        Test month rollover
        """
        
        init = "2020-Feb-29 23:59:00"
        init = eph.oneMinIncr(init)
        
        test = "2020-Mar-01 00:00:00"
        
        self.assertEqual(init, test)
    
    
    def test_1_oneMinIncr3(self):
        """
        Test year rollover
        """
        
        init = "2020-Dec-31 23:59:00"
        init = eph.oneMinIncr(init)
        
        test = "2021-Jan-01 00:00:00"
        
        self.assertEqual(init, test)
    
    
    # ----|getEph()|----
    def test_1_getEph1(self):
        """
        Test retrieving single ephemeris
        """
        
        # Define fileSpec
        fileSpec = "".join([EPHDIR, self.SINGLEFILE])
        
        # Create Ephemeris
        eph.getEph(self.SINGLEBODY, self.START, self.SINGLEFILE)
        
        # Test that file exists
        self.assertTrue(os.path.exists(fileSpec))
        
        # Test that file was created as expected
        with open(fileSpec, "r") as ephFile:
            ephemeris = ephFile.readlines()
        
        # Known Uranus Position on 2001-Jan-01 12:00:00.0000:
        # Uranus
        # 2451911.000000000 = A.D. 2001-Jan-01 12:00:00.0000 TDB 
        #  X = 2.654328176363667E+09 Y =-3.638817729312420E+09 Z = 1.377102421198058E+07
        #  VX= 4.342357634772094E+00 VY= 3.240217750846927E+00 VZ=-1.666811061909046E-01
        #  LT= 1.502395871745598E+04 RG= 4.504069512796657E+09 RR=-5.923863865380654E-02
        times = ["2451911.000000000 = A.D. 2001-Jan-01 12:00:00.0000 TDB \n"]
        positions = [" X = 2.300003607494758E+09 Y =-1.903352788133292E+09 Z =-3.688904950700366E+07\n"]
        velocities = [" VX= 4.279549179385135E+00 VY= 4.934973236529308E+00 VZ=-3.719841029726623E-02\n"]
        magnitudes = [" LT= 9.959069429557150E+03 RG= 2.985653903679596E+09 RR= 1.511748214917280E-01\n"]
        
        self.assertEqual(len(ephemeris), 5*len(self.SINGLEBODY))
        for i in range(len(self.SINGLEBODY)):
            # Check name
            self.assertEqual(ephemeris[i*5], self.SINGLEBODY[i] + "\n")
            # Check time
            self.assertEqual(ephemeris[i*5 + 1], times[i])
            # Check position, velocity, and magnitudes
            self.assertEqual(ephemeris[i*5 + 2], positions[i])
            self.assertEqual(ephemeris[i*5 + 3], velocities[i])
            self.assertEqual(ephemeris[i*5 + 4], magnitudes[i])
        
    
    def test_1_getEph2(self):
        """
        Test retrieving the 8 major planet ephemerides
        """
        
        # Define ephemeris properties
        fileSpec = "".join([EPHDIR, self.MULTIFILE])
        
        # Create Ephemeris
        eph.getEph(self.MULTIBODY, self.START, self.MULTIFILE)
        
        # Test that file exists
        self.assertTrue(os.path.exists(fileSpec))
        
        # Test that file was created as expected
        with open(fileSpec, "r") as ephFile:
            ephemeris = ephFile.readlines()
        
        # Known values
        # Mercury
        # 2451911.000000000 = A.D. 2001-Jan-01 12:00:00.0000 TDB 
        #  X = 2.675472781979483E+07 Y =-6.034803219031359E+07 Z =-7.385481471819222E+06
        #  VX= 3.478133806905820E+01 VY= 2.219213371024880E+01 VZ=-1.379458464408147E+00
        #  LT= 2.215690754978997E+02 RG= 6.642473776030293E+07 RR=-5.999246888710180E+00
        # Venus
        # 2451911.000000000 = A.D. 2001-Jan-01 12:00:00.0000 TDB 
        #  X = 7.283234410001498E+07 Y = 7.986004273075339E+07 Z =-3.112397373885911E+06
        #  VX=-2.598288208967642E+01 VY= 2.344857542046295E+01 VZ= 1.820232447919288E+00
        #  LT= 3.606792605727311E+02 RG= 1.081289220767215E+08 RR=-2.354158370752044E-01
        # Earth
        # 2451911.000000000 = A.D. 2001-Jan-01 12:00:00.0000 TDB 
        #  X =-2.841738149052626E+07 Y = 1.443297455185989E+08 Z = 6.360703883320093E+01
        #  VX=-2.971395108252550E+01 VY=-5.876328555499981E+00 VZ= 3.668470273869850E-04
        #  LT= 4.906751991643584E+02 RG= 1.471007240371225E+08 RR=-2.539974903011847E-02
        # Mars
        # 2451911.000000000 = A.D. 2001-Jan-01 12:00:00.0000 TDB 
        #  X =-2.463609631805802E+08 Y =-9.561917031075299E+06 Z = 5.854945724576869E+06
        #  VX= 1.848537430108924E+00 VY=-2.214369242722758E+01 VZ=-5.092797459809653E-01
        #  LT= 8.226223165481807E+02 RG= 2.466159662836332E+08 RR=-1.000150643100931E+00
        # Jupiter
        # 2451911.000000000 = A.D. 2001-Jan-01 12:00:00.0000 TDB 
        #  X = 2.682560022363798E+08 Y = 7.055711580224835E+08 Z =-8.934894593638599E+06
        #  VX=-1.239154257755430E+01 VY= 5.265175348708839E+00 VZ= 2.554630194282039E-01
        #  LT= 2.518070414836225E+03 RG= 7.548985190808315E+08 RR= 5.147283371350162E-01
        # Saturn
        # 2451911.000000000 = A.D. 2001-Jan-01 12:00:00.0000 TDB 
        #  X = 7.005991996174215E+08 Y = 1.168568577827738E+09 Z =-4.819416918317384E+07
        #  VX=-8.805707541551364E+00 VY= 4.952405759906005E+00 VZ= 2.636491214206345E-01
        #  LT= 4.547635345000812E+03 RG= 1.363346778165471E+09 RR=-2.895464724614246E-01
        # Uranus
        # 2451911.000000000 = A.D. 2001-Jan-01 12:00:00.0000 TDB 
        #  X = 2.300003607494758E+09 Y =-1.903352788133292E+09 Z =-3.688904950700366E+07
        #  VX= 4.279549179385135E+00 VY= 4.934973236529308E+00 VZ=-3.719841029726623E-02
        #  LT= 9.959069429557150E+03 RG= 2.985653903679596E+09 RR= 1.511748214917280E-01
        # Neptune
        # 2451911.000000000 = A.D. 2001-Jan-01 12:00:00.0000 TDB 
        #  X = 2.654328176363667E+09 Y =-3.638817729312420E+09 Z = 1.377102421198058E+07
        #  VX= 4.342357634772094E+00 VY= 3.240217750846927E+00 VZ=-1.666811061909046E-01
        #  LT= 1.502395871745598E+04 RG= 4.504069512796657E+09 RR=-5.923863865380654E-02

        times = ["2451911.000000000 = A.D. 2001-Jan-01 12:00:00.0000 TDB \n"]
        times *= len(self.MULTIBODY)
        positions =  [" X = 2.675472781979483E+07 Y =-6.034803219031359E+07 Z =-7.385481471819222E+06\n",
                      " X = 7.283234410001498E+07 Y = 7.986004273075339E+07 Z =-3.112397373885911E+06\n",
                      " X =-2.841738149052626E+07 Y = 1.443297455185989E+08 Z = 6.360703883320093E+01\n",
                      " X =-2.463609631805802E+08 Y =-9.561917031075299E+06 Z = 5.854945724576869E+06\n",
                      " X = 2.682560022363798E+08 Y = 7.055711580224835E+08 Z =-8.934894593638599E+06\n",
                      " X = 7.005991996174215E+08 Y = 1.168568577827738E+09 Z =-4.819416918317384E+07\n",
                      " X = 2.300003607494758E+09 Y =-1.903352788133292E+09 Z =-3.688904950700366E+07\n",
                      " X = 2.654328176363667E+09 Y =-3.638817729312420E+09 Z = 1.377102421198058E+07\n"]
        velocities = [" VX= 3.478133806905820E+01 VY= 2.219213371024880E+01 VZ=-1.379458464408147E+00\n",
                      " VX=-2.598288208967642E+01 VY= 2.344857542046295E+01 VZ= 1.820232447919288E+00\n",
                      " VX=-2.971395108252550E+01 VY=-5.876328555499981E+00 VZ= 3.668470273869850E-04\n",
                      " VX= 1.848537430108924E+00 VY=-2.214369242722758E+01 VZ=-5.092797459809653E-01\n",
                      " VX=-1.239154257755430E+01 VY= 5.265175348708839E+00 VZ= 2.554630194282039E-01\n",
                      " VX=-8.805707541551364E+00 VY= 4.952405759906005E+00 VZ= 2.636491214206345E-01\n",
                      " VX= 4.279549179385135E+00 VY= 4.934973236529308E+00 VZ=-3.719841029726623E-02\n",
                      " VX= 4.342357634772094E+00 VY= 3.240217750846927E+00 VZ=-1.666811061909046E-01\n"]
        magnitudes = [" LT= 2.215690754978997E+02 RG= 6.642473776030293E+07 RR=-5.999246888710180E+00\n",
                      " LT= 3.606792605727311E+02 RG= 1.081289220767215E+08 RR=-2.354158370752044E-01\n",
                      " LT= 4.906751991643584E+02 RG= 1.471007240371225E+08 RR=-2.539974903011847E-02\n",
                      " LT= 8.226223165481807E+02 RG= 2.466159662836332E+08 RR=-1.000150643100931E+00\n",
                      " LT= 2.518070414836225E+03 RG= 7.548985190808315E+08 RR= 5.147283371350162E-01\n",
                      " LT= 4.547635345000812E+03 RG= 1.363346778165471E+09 RR=-2.895464724614246E-01\n",
                      " LT= 9.959069429557150E+03 RG= 2.985653903679596E+09 RR= 1.511748214917280E-01\n",
                      " LT= 1.502395871745598E+04 RG= 4.504069512796657E+09 RR=-5.923863865380654E-02\n"]
        
        self.assertEqual(len(ephemeris), 5*len(self.MULTIBODY))
        for i in range(len(self.MULTIBODY)):
            # Check name
            self.assertEqual(ephemeris[i*5], self.MULTIBODY[i] + "\n")
            # Check time
            self.assertEqual(ephemeris[i*5 + 1], times[i])
            # Check position, velocity, and magnitudes
            self.assertEqual(ephemeris[i*5 + 2], positions[i])
            self.assertEqual(ephemeris[i*5 + 3], velocities[i])
            self.assertEqual(ephemeris[i*5 + 4], magnitudes[i])
    
    
    # ----|createEph()|----
    def test_2_createEph1(self):
        """
        Test creating single body ephemeris 2D dictionary from file
        """
        
        # Create fileSpec for createEph
        fileSpec = "".join([EPHDIR, self.SINGLEFILE])
        
        # Test that the file exists
        exists = os.path.exists(fileSpec)
        self.assertTrue(exists)
        
        # Create Ephemeris Dict
        # Uranus
        # 2451911.000000000 = A.D. 2001-Jan-01 12:00:00.0000 TDB 
        #  X = 2.300003607494758E+09 Y =-1.903352788133292E+09 Z =-3.688904950700366E+07
        #  VX= 4.279549179385135E+00 VY= 4.934973236529308E+00 VZ=-3.719841029726623E-02
        #  LT= 9.959069429557150E+03 RG= 2.985653903679596E+09 RR= 1.511748214917280E-01
        ephemeris = eph.createEph(self.SINGLEFILE)
        name = self.SINGLEBODY[0]
        test = {name:{"center": np.array([2.300003607494758E+09,
                                          -1.903352788133292E+09,
                                          -3.688904950700366E+07])*1E3,
                      "velocity": np.array([4.279549179385135E+00,
                                            4.934973236529308E+00,
                                            -3.719841029726623E-02])*1E3}}
        
        # Test the subdictionaries
        for body in test.keys():
            # Test that body is in the created ephemeris
            self.assertTrue(body in ephemeris.keys())
            
            # Test that body dictionary is created correctly
            for i in range(3):
                # Check Center
                self.assertTrue(abs(ephemeris[body]["center"][i] -
                                    test[body]["center"][i]) < self.TOL)
                
                # Check Velocity
                self.assertTrue(abs(ephemeris[body]["velocity"][i] -
                                    test[body]["velocity"][i]) < self.TOL)
        
        # The ephemeris file is not cleaned up here, and instead in the single
        # planet creation method because the file is also used there.
    
    
    def test_2_createEph2(self):
        """
        Test creating 8 body ephemeris 2D dictionary from file
        """
        
        numBodies = len(self.MULTIBODY)
        fileName = "".join([CURTIME, "-", str(numBodies),
                           ["body", "bodies"][numBodies > 1],
                           ".txt"])
        fileSpec = "".join([EPHDIR, fileName])
        
        # Test that the file exists
        exists = os.path.exists(fileSpec)
        self.assertTrue(exists)
        
        # Create Ephemeris Dict
        # Mercury
        # 2451911.000000000 = A.D. 2001-Jan-01 12:00:00.0000 TDB 
        #  X = 2.675472781979483E+07 Y =-6.034803219031359E+07 Z =-7.385481471819222E+06
        #  VX= 3.478133806905820E+01 VY= 2.219213371024880E+01 VZ=-1.379458464408147E+00
        #  LT= 2.215690754978997E+02 RG= 6.642473776030293E+07 RR=-5.999246888710180E+00
        # Venus
        # 2451911.000000000 = A.D. 2001-Jan-01 12:00:00.0000 TDB 
        #  X = 7.283234410001498E+07 Y = 7.986004273075339E+07 Z =-3.112397373885911E+06
        #  VX=-2.598288208967642E+01 VY= 2.344857542046295E+01 VZ= 1.820232447919288E+00
        #  LT= 3.606792605727311E+02 RG= 1.081289220767215E+08 RR=-2.354158370752044E-01
        # Earth
        # 2451911.000000000 = A.D. 2001-Jan-01 12:00:00.0000 TDB 
        #  X =-2.841738149052626E+07 Y = 1.443297455185989E+08 Z = 6.360703883320093E+01
        #  VX=-2.971395108252550E+01 VY=-5.876328555499981E+00 VZ= 3.668470273869850E-04
        #  LT= 4.906751991643584E+02 RG= 1.471007240371225E+08 RR=-2.539974903011847E-02
        # Mars
        # 2451911.000000000 = A.D. 2001-Jan-01 12:00:00.0000 TDB 
        #  X =-2.463609631805802E+08 Y =-9.561917031075299E+06 Z = 5.854945724576869E+06
        #  VX= 1.848537430108924E+00 VY=-2.214369242722758E+01 VZ=-5.092797459809653E-01
        #  LT= 8.226223165481807E+02 RG= 2.466159662836332E+08 RR=-1.000150643100931E+00
        # Jupiter
        # 2451911.000000000 = A.D. 2001-Jan-01 12:00:00.0000 TDB 
        #  X = 2.682560022363798E+08 Y = 7.055711580224835E+08 Z =-8.934894593638599E+06
        #  VX=-1.239154257755430E+01 VY= 5.265175348708839E+00 VZ= 2.554630194282039E-01
        #  LT= 2.518070414836225E+03 RG= 7.548985190808315E+08 RR= 5.147283371350162E-01
        # Saturn
        # 2451911.000000000 = A.D. 2001-Jan-01 12:00:00.0000 TDB 
        #  X = 7.005991996174215E+08 Y = 1.168568577827738E+09 Z =-4.819416918317384E+07
        #  VX=-8.805707541551364E+00 VY= 4.952405759906005E+00 VZ= 2.636491214206345E-01
        #  LT= 4.547635345000812E+03 RG= 1.363346778165471E+09 RR=-2.895464724614246E-01
        # Uranus
        # 2451911.000000000 = A.D. 2001-Jan-01 12:00:00.0000 TDB 
        #  X = 2.300003607494758E+09 Y =-1.903352788133292E+09 Z =-3.688904950700366E+07
        #  VX= 4.279549179385135E+00 VY= 4.934973236529308E+00 VZ=-3.719841029726623E-02
        #  LT= 9.959069429557150E+03 RG= 2.985653903679596E+09 RR= 1.511748214917280E-01
        # Neptune
        # 2451911.000000000 = A.D. 2001-Jan-01 12:00:00.0000 TDB 
        #  X = 2.654328176363667E+09 Y =-3.638817729312420E+09 Z = 1.377102421198058E+07
        #  VX= 4.342357634772094E+00 VY= 3.240217750846927E+00 VZ=-1.666811061909046E-01
        #  LT= 1.502395871745598E+04 RG= 4.504069512796657E+09 RR=-5.923863865380654E-02
        ephemeris = eph.createEph(fileName)
        
        test = {"Mercury":{"center":np.array([2.675472781979483E+07,
                                              -6.034803219031359E+07,
                                              -7.385481471819222E+06])*1E3,
                           "velocity":np.array([3.478133806905820E+01,
                                                2.219213371024880E+01,
                                                -1.379458464408147E+00])*1E3},
                "Venus":  {"center":np.array([7.283234410001498E+07,
                                              7.986004273075339E+07,
                                              -3.112397373885911E+06])*1E3,
                           "velocity":np.array([-2.598288208967642E+01,
                                                2.344857542046295E+01,
                                                1.820232447919288E+00])*1E3},
                "Earth":  {"center":np.array([-2.841738149052626E+07,
                                              1.443297455185989E+08,
                                              6.360703883320093E+01])*1E3,
                           "velocity":np.array([-2.971395108252550E+01,
                                                -5.876328555499981E+00,
                                                3.668470273869850E-04])*1E3},
                "Mars":   {"center":np.array([-2.463609631805802E+08,
                                              -9.561917031075299E+06,
                                              5.854945724576869E+06])*1E3,
                           "velocity":np.array([1.848537430108924E+00,
                                                -2.214369242722758E+01,
                                                -5.092797459809653E-01])*1E3},
                "Jupiter":{"center":np.array([2.682560022363798E+08,
                                              7.055711580224835E+08,
                                              -8.934894593638599E+06])*1E3,
                           "velocity":np.array([-1.239154257755430E+01,
                                                5.265175348708839E+00,
                                                2.554630194282039E-01])*1E3},
                "Saturn": {"center":np.array([7.005991996174215E+08,
                                              1.168568577827738E+09,
                                              -4.819416918317384E+07])*1E3,
                           "velocity":np.array([-8.805707541551364E+00,
                                                4.952405759906005E+00,
                                                2.636491214206345E-01])*1E3},
                "Uranus": {"center":np.array([2.300003607494758E+09,
                                              -1.903352788133292E+09,
                                              -3.688904950700366E+07])*1E3,
                           "velocity":np.array([4.279549179385135E+00,
                                                4.934973236529308E+00,
                                                -3.719841029726623E-02])*1E3},
                "Neptune":{"center":np.array([2.654328176363667E+09,
                                              -3.638817729312420E+09,
                                              1.377102421198058E+07])*1E3,
                           "velocity":np.array([4.342357634772094E+00,
                                                3.240217750846927E+00,
                                                -1.666811061909046E-01])*1E3}}
        
        # Test each sub-dictionary
        for body in test.keys():
            # Test that body is in the created ephemeris
            self.assertTrue(body in ephemeris.keys())
            
            # Test that body dictionary is created correctly
            for i in range(3):
                # Check Center
                self.assertTrue(abs(ephemeris[body]["center"][i] -
                                    test[body]["center"][i]) < self.TOL)
                
                # Check Velocity
                self.assertTrue(abs(ephemeris[body]["velocity"][i] -
                                    test[body]["velocity"][i]) < self.TOL)
        
        # Remove file to prevent file build up
        # if os.path.exists(fileSpec):
        #     os.remove(fileSpec)
    
    
    # --|orbital.py|----------------------------------------------------------
    
    
    # --|prop.py|-------------------------------------------------------------
    
    
# ----------------------------------------------------------------------------


# --|Run|---------------------------------------------------------------------
if __name__ == "__main__":
    main()