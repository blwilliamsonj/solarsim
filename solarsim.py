# ----------------------------------------------------------------------------
# Bryan Williamson
# 
# --|solarsim.py|-------------------------------------------------------------
# solarsim.py handles the user interface of the solarsim program.
# Reliant on tkinter
# 
# ----|Classes|----
# Solarsim - The main GUI for the solarsim program. Handles user inputs and
#       simulation calls.
# 
# Graph - Window for displaying simulation results.
# ----------------------------------------------------------------------------

# --|Imports|-----------------------------------------------------------------
import tkinter as tk
from tkinter import ttk
import matplotlib
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg,
                                               NavigationToolbar2Tk)
import numpy as np
from os.path import dirname, realpath, exists
from sys import path
from calendar import isleap
from datetime import datetime, timedelta

# --|GLOBALS|-----------------------------------------------------------------
PROJDIR = dirname(realpath(__file__))
LIBDIR = "/".join([PROJDIR, "lib/"])
EPHDIR = "/".join([PROJDIR, "eph/"])
RESDIR = "/".join([PROJDIR, "res/"])
RESULTSDIR = "/".join([PROJDIR, "results/"])


# --|Solarsim Imports|--------------------------------------------------------
path.insert(0, LIBDIR)
import spaceObj as so
import prop as pr
import getEph as eph


# --|Solarsim Class|----------------------------------------------------------
class Solarsim:
    """
    The Solarsim class handles the user interface of the solarsim program.
    """
    
    # ----|CLASS VARIABLES|---------------------------------------------------
    # Window Sizes
    WINDOW_WIDTH, WINDOW_HEIGHT = 800, 408
    ROWS, COLS = 4, 2
    ROWSIZES = [3,8,10,3]
    COLSIZES = [2,3]
    
    # GUI Sections
    GUI_TIME = "time"
    GUI_POINTMASS = "pointMass"
    GUI_PLANETS = "planets"
    GUI_CUSTOM = "customPlanets"
    GUI_STARTRESET = "startReset"
    
    # Images
    IMG_WINDOWICON = "".join([RESDIR, "icon.ico"])
    IMG_LOGO = "".join([RESDIR, "logo.png"])
    
    # Colors
    COLOR_MAIN = "#FFFFFF"
    COLOR_ACCENT = "#1D2A36"
    COLOR_ACTION = "#74C47C"
    COLOR_MAINTEXT = "#000000"
    COLOR_SUBTEXT = "#E5E5E5"
    
    # Fonts
    FONT_MAIN = "Calibri"
    FONT_SIZE_HEADER = 16
    FONT_SIZE_MAIN = 12
    
    # Time Options
    YEARS = (0,9999)
    MONTHS = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
              "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    DAYS = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31, 29]
    HOURS = [i for i in range(24)]
    MINUTES = [i for i in range(60)]
    TIMEFORMAT = "%Y-%b-%d %H:%M:%S"
    
    # Planets
    PLANETS = ["Mercury", "Venus", "Earth", "Mars", "Jupiter",
               "Saturn", "Uranus", "Neptune", "Pluto", "Custom"]
    
    
    # ----|Methods|-----------------------------------------------------------
    def __init__(self, draw = True):
        """
        Handles creating the Solarsim GUI.
        """
        
        # ----|Initialize and Configure the Window|----
        # Init
        self.window = tk.Tk()
        
        self.window.geometry("x".join([str(self.WINDOW_WIDTH),
                                       str(self.WINDOW_HEIGHT)]))
        self.window.resizable(False, False)
        self.window.protocol("WM_DELETE_WINDOW", self.close)
        
        # Config
        self.window.title("solarsim")
        self.window.iconbitmap(self.IMG_WINDOWICON)
        self.window.configure(bg = self.COLOR_ACCENT)
        
        # ----|Define TTK Style|----
        self.style = ttk.Style(self.window)
        
        # Combobox
        self.style.configure("ss.TCombobox",
                             foreground = self.COLOR_MAINTEXT,
                             background = self.COLOR_MAIN)
        self.window.option_add("*TCombobox*Listbox*Font",
                               (self.FONT_MAIN, self.FONT_SIZE_MAIN))
        self.window.option_add("*TCombobox*Listbox*selectForeground",
                               self.COLOR_MAIN)
        self.window.option_add("*TCombobox*Listbox*selectBackground",
                               self.COLOR_ACCENT)
        self.window.option_add("*TCombobox*Listbox*foreground",
                               self.COLOR_MAINTEXT)
        self.window.option_add("*TCombobox*Listbox*background",
                               self.COLOR_MAIN)
        
        # Checkbutton
        self.style.configure("ss.TCheckbutton",
                             foreground = self.COLOR_MAINTEXT,
                             background = self.COLOR_MAIN,
                             font = (self.FONT_MAIN, self.FONT_SIZE_MAIN))
        
        # Button
        self.style.configure("ss.TButton",
                             foreground = self.COLOR_MAINTEXT,
                             background = self.COLOR_MAIN,
                             font = (self.FONT_MAIN, self.FONT_SIZE_MAIN))
        self.style.map("ss.TButton",
                       foreground = [("active", self.COLOR_MAINTEXT),
                                     ("disabled", self.COLOR_MAIN)])
        
        
        # ----|Initialize Sim Variables|----
        # Sim Time and Length
        self.startYear = tk.StringVar(self.window)
        self.startMonth = tk.StringVar(self.window)
        self.startDay = tk.StringVar(self.window)
        self.startHour = tk.StringVar(self.window)
        self.startMinute = tk.StringVar(self.window)
        self.startSeconds = tk.StringVar(self.window)
        self.simLength = tk.StringVar(self.window)
        self.validSimTimes = False
        
        # Point Mass
        self.insertPointMass = tk.IntVar(self.window)
        self.pointR = [tk.StringVar(self.window) for i in range(3)]
        self.pointV = [tk.StringVar(self.window) for i in range(3)]
        self.pointM = tk.StringVar(self.window)
        
        # Planets
        #  0 for unselected planet, 1 for selected planet
        #  Planets in the following order:
        #       [Mercury, Venus, Earth, Mars, Jupiter,
        #        Saturn, Uranus, Neptune, Pluto, Custom]
        self.chosenPlanets = [tk.IntVar(self.window) for i in range(10)]
        self.numCustomPlanets = tk.StringVar(self.window)
        
        # Custom Planets
        self.curCustomPlanet = tk.StringVar(self.window)
        self.customPlanetProperties = [{"center": [tk.StringVar(self.window)
                                                     for i in range(3)],
                                        "velocity": [tk.StringVar(self.window)
                                                     for i in range(3)],
                                        "mass": tk.StringVar(self.window),
                                        "radius": tk.StringVar(self.window)}
                                       for i in range(10)]
        
        # Useful Variables
        self.tol = 0.00001 # % tolerance for the corrector loop
        self.defaultDisabled = {self.GUI_TIME: ["menu_startMonth",
                                                "menu_startDay",
                                                "menu_startHour",
                                                "menu_startMinute",
                                                "menu_startSecond",
                                                "txt_simLength"],
                                self.GUI_PLANETS: ["menu_numCustomPlanets"],
                                self.GUI_POINTMASS: ["txt_pointMassRX",
                                                     "txt_pointMassRY",
                                                     "txt_pointMassRZ",
                                                     "txt_pointMassVX",
                                                     "txt_pointMassVY",
                                                     "txt_pointMassVZ",
                                                     "txt_pointMassM"],
                                self.GUI_CUSTOM: ["menu_customPlanetNum",
                                                  "txt_planetRX",
                                                  "txt_planetVX",
                                                  "txt_planetRY",
                                                  "txt_planetVY",
                                                  "txt_planetRZ",
                                                  "txt_planetVZ",
                                                  "txt_planetM",
                                                  "txt_planetR"]}
        self.inputVars = {self.GUI_TIME: [self.startYear,
                                          self.startMonth,
                                          self.startDay,
                                          self.startHour,
                                          self.startMinute,
                                          self.startSeconds,
                                          self.simLength],
                          self.GUI_PLANETS: [self.chosenPlanets,
                                             self.numCustomPlanets],
                          self.GUI_POINTMASS: [self.insertPointMass,
                                               self.pointR,
                                               self.pointV,
                                               self.pointM],
                          self.GUI_CUSTOM: [self.curCustomPlanet,
                                            self.customPlanetProperties]}
        
        
        # Draw and retrieve GUI elements
        self.gui = dict()
        self.mainGrid()
        
        # Start GUI if drawing is desired
        if draw:
            self.window.mainloop()
    
    
    def mainGrid(self):
        """
        mainGrid handles the creation of the tkinter grid for placing elements
        of the application. The various gui elements created during the
        sub-methods of mainGrid are added to the object's gui dictionary as
        sub-dictionaries.
        """
        
        # Loop through grid values
        for row in range(self.ROWS):
            for col in range(self.COLS):
                # Set Default Grid Values
                sticky = "NSEW"
                rowspan = 1
                colspan = 1
                
                # Col 1
                if col == 0:
                    # Row 1 - Logo/Info Text
                    if row == 0:
                        height = int(self.WINDOW_HEIGHT*(self.ROWSIZES[row]/
                                                         sum(self.ROWSIZES)))
                        
                        item = tk.Frame(master = self.window,
                                        bg = self.COLOR_MAIN,
                                        height = height,
                                        width = self.WINDOW_WIDTH)
                        
                        item.rowconfigure(0, weight = 1)
                        item.columnconfigure(0, weight = 1)
                        
                        tk.Label(master = item,
                                 text = "Configure Simulation",
                                 font = (self.FONT_MAIN,
                                         self.FONT_SIZE_HEADER + 2),
                                 fg = self.COLOR_MAINTEXT,
                                 bg = self.COLOR_MAIN).grid(row = 0,
                                                            column = 0,
                                                            sticky = "W")
                        
                        padx, pady = 0, (0,1)
                        colspan = 2
                        
                    
                    # Row 2 - Time Configuration
                    elif row == 1:
                        padx, pady = 0, (0,1)
                        item = self.timeConfigUI(row, col)
                    
                    # Row 3 - Point Mass Configuration
                    elif row == 2:
                        padx, pady = 0, 0
                        item = self.pointMassConfigUI(row, col)
                    
                    # Row 4 - Blank
                    else:
                        padx, pady = 0, 0
                        item = tk.Frame(master = self.window,
                                        bg = self.COLOR_MAIN)
                    
                
                # Col 2
                else:
                    # Row 1 - Blank
                    if row == 0:
                        # This Column is merged with the one to the left
                        continue
                        
                    
                    # Row 2 - Planet Configuration
                    elif row == 1:
                        # This row is merged with the above
                        padx, pady = (1,0), (0,1)
                        item = self.planetConfigUI(row, col)
                    
                    # Row 3 - Custom Planet Configuration
                    elif row == 2:
                        padx, pady = (1,0), 0
                        item = self.customPlanetConfigUI(row, col)
                    
                    # Row 4 - Start/Reset Buttons
                    else:
                        padx, pady = (1,0), 0
                        sticky = "SE"
                        item = self.startResetUI(row, col)
                
                # Place item in the grid
                item.grid(row = row, column = col,
                          sticky = sticky,
                          rowspan = rowspan, columnspan = colspan,
                          padx = padx, pady = pady)
                item.grid_propagate(False)
    
    
    # ----|UI Element Creation Methods|----
    def timeConfigUI(self, row, col):
        """
        timeConfigUI returns a frame containing all the items needed to
        configure the time of the simulation. The object's gui dictionary is
        modified to contain the ui elements added by this method.
        
        INPUT:
        row : int
            Integer defining the row within the main window this portion of
            the GUI will inhabit.
        col : int
            Integer defining the column within the main window this portion of
            the GUI will inhabit
        
        OUTPUT:
        timeConfig : tkinter Frame
            Tkinter Frame containing the time configuration UI elements.
        """
        
        # ----|Add Traces to Input vars|----
        self.startMonth.trace_add("write", self.checkMonth)
        self.startDay.trace_add("write", self.checkDay)
        self.startHour.trace_add("write", self.checkHour)
        self.startMinute.trace_add("write", self.checkMinute)
        self.startSeconds.trace_add("write", self.checkSeconds)
        
        
        # ----|Define Frames|----
        # Create Frames
        height = int(self.WINDOW_HEIGHT*(self.ROWSIZES[row]/
                                         sum(self.ROWSIZES)))
        width = int(self.WINDOW_WIDTH*(self.COLSIZES[col]/
                                       sum(self.COLSIZES)))
        timeConfig = tk.Frame(master = self.window,
                              bg = self.COLOR_MAIN,
                              height = height, width = width)
        section = tk.Frame(master = timeConfig, bg = self.COLOR_MAIN)
        start = tk.Frame(master = timeConfig, bg = self.COLOR_MAIN)
        simLen = tk.Frame(master = timeConfig, bg = self.COLOR_MAIN)
        
        # Configure Rows and Columns
        timeConfig.rowconfigure(0, weight = 1)
        timeConfig.rowconfigure(1, weight = 2)
        timeConfig.rowconfigure(2, weight = 2)
        timeConfig.columnconfigure(0, weight = 1)
        
        
        # ----|Create GUI Items|----
        # Year
        yearValidate = (self.window.register(self.checkYear),
                        "%d", "%i", "%P", "%s", "%S")
        startYearEntry = tk.Entry(master = start,
                                  bg = self.COLOR_MAIN,
                                  fg = "red",
                                  disabledbackground = self.COLOR_SUBTEXT,
                                  textvariable = self.startYear,
                                  validate = "key",
                                  validatecommand = yearValidate,
                                  font = (self.FONT_MAIN,
                                          self.FONT_SIZE_MAIN),
                                  width = 6)
        # Month
        startMonthMenu = ttk.Combobox(master = start,
                                      textvariable = self.startMonth,
                                      values = self.MONTHS,
                                      font = (self.FONT_MAIN,
                                              self.FONT_SIZE_MAIN),
                                      style = "ss.TCombobox",
                                      width = 4)
        startMonthMenu.config(state = "disabled")
        
        # Day
        startDayMenu = ttk.Combobox(master = start,
                                    textvariable = self.startDay,
                                    values = [0],
                                    font = (self.FONT_MAIN,
                                            self.FONT_SIZE_MAIN),
                                    style = "ss.TCombobox",
                                    width = 4)
        startDayMenu.config(state = "disabled")
        
        # Hour
        startHourMenu = ttk.Combobox(master = start,
                                     textvariable = self.startHour,
                                     values = self.HOURS,
                                     font = (self.FONT_MAIN,
                                             self.FONT_SIZE_MAIN),
                                     style = "ss.TCombobox",
                                     width = 4)
        startHourMenu.config(state = "disabled")
        
        # Minute
        startMinuteMenu = ttk.Combobox(master = start,
                                       textvariable = self.startMinute,
                                       values = self.MINUTES,
                                       font = (self.FONT_MAIN,
                                               self.FONT_SIZE_MAIN),
                                       style = "ss.TCombobox",
                                       width = 4)
        startMinuteMenu.config(state = "disabled")
        
        # Second
        startSecondMenu = ttk.Combobox(master = start,
                                       textvariable = self.startSeconds,
                                       values = self.MINUTES,
                                       font = (self.FONT_MAIN,
                                               self.FONT_SIZE_MAIN),
                                       style = "ss.TCombobox",
                                       width = 4)
        startSecondMenu.config(state = "disabled")
        
        # Sim Length
        simLenValidate = (self.window.register(self.checkSimLen),
                          "%d", "%i", "%P", "%s", "%S")
        simLengthEntry = tk.Entry(master = simLen,
                                  bg = self.COLOR_MAIN,
                                  fg = self.COLOR_MAINTEXT,
                                  disabledbackground = self.COLOR_SUBTEXT,
                                  disabledforeground = self.COLOR_ACCENT,
                                  textvariable = self.simLength,
                                  validate = "key",
                                  validatecommand = simLenValidate,
                                  font = (self.FONT_MAIN,
                                          self.FONT_SIZE_MAIN),
                                  width = 6)
        simLengthEntry.config(state = "disabled")
      
        
        # ----|Place GUI Items|----
        """
        WOULD LIKE TO CHANGE THIS SECTION TO A FOR LOOP FOR CODE COMPACTION
        """
        
        # Section Label
        tk.Label(master = section,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                 font = (self.FONT_MAIN, self.FONT_SIZE_HEADER),
                 text = "Time:").grid(row = 0, column = 0,
                                      sticky = "W", padx = 3)
        
        # Year
        tk.Label(master = start, width = 1,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                 font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                 text = "Y:").grid(row = 0, column = 0, padx = 3,
                                   sticky = "W")
        startYearEntry.grid(row = 0, column = 1, ipadx = 2)
        
        # Month
        tk.Label(master = start, width = 2,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                 font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                 text = "M:").grid(row = 0, column = 2, padx = 2,
                                   sticky = "W")
        startMonthMenu.grid(row = 0, column = 3)
        
        # Day
        tk.Label(master = start, width = 2,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                 font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                 text = "D:").grid(row = 0, column = 4, padx = 2,
                                   sticky = "W")
        startDayMenu.grid(row = 0, column = 5)
        
        # Hour
        tk.Label(master = start, width = 1,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                 font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                 text = "H:").grid(row = 2, column = 0, padx = 3,
                                   sticky = "W")
        startHourMenu.grid(row = 2, column = 1)
        
        # Minute
        tk.Label(master = start, width = 2,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                 font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                 text = "M:").grid(row = 2, column = 2, padx= 2,
                                   sticky = "W")
        startMinuteMenu.grid(row = 2, column = 3)
        
        # Second
        tk.Label(master = start, width = 2,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                 font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                 text = "S:").grid(row = 2, column = 4, padx= 2,
                                   sticky = "W")
        startSecondMenu.grid(row = 2, column = 5)
        
        # Sim Length
        tk.Label(master = simLen, width = 15,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                 font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                 text = "Sim Length (Days):\nMax: 20,000 ",
                 justify = "right").grid(row = 0, column = 0,
                                         padx= (0,1), sticky = "NW")
        simLengthEntry.grid(row = 0, column = 1, sticky = "E",
                            padx = (0,2), pady = 3, ipadx = 2)
        
        # Frames
        section.grid(row = 0, column = 0, sticky = "NW")
        start.grid(row = 1, column = 0, sticky = "SW")
        simLen.grid(row = 2, column = 0, sticky = "N", padx = (0,22))
        
        
        # ----|Add Elements to self.gui|----
        self.gui[self.GUI_TIME] = {"txt_startYear": startYearEntry,
                                   "menu_startMonth": startMonthMenu,
                                   "menu_startDay": startDayMenu,
                                   "menu_startHour": startHourMenu,
                                   "menu_startMinute": startMinuteMenu,
                                   "menu_startSecond": startSecondMenu,
                                   "txt_simLength": simLengthEntry}
        
        return timeConfig
    
    
    def pointMassConfigUI(self, row, col):
        """
        pointMassConfigUI returns a frame containing all the items needed to
        configure the point mass for the simulation. The object's gui
        dictionary is modified to contain the ui elements added by
        this method.
        
        INPUT:
        row : int
            Integer defining the row within the main window this portion of
            the GUI will inhabit.
        col : int
            Integer defining the column within the main window this portion of
            the GUI will inhabit

        OUTPUT:
        pointMassConfig : tkinter Frame
            Tkinter Frame containing the point mass configuration UI elements.
        """
        
        # ----|Create Dictionary|----
        self.gui[self.GUI_POINTMASS] = dict()
        
        # ----|Define Frames|----
        # Create Frames
        height = int(self.WINDOW_HEIGHT*(self.ROWSIZES[row]/
                                          sum(self.ROWSIZES)))
        width = int(self.WINDOW_WIDTH*(self.COLSIZES[col]/
                                          sum(self.COLSIZES)))
        
        pointMassConfig = tk.Frame(master = self.window,
                                   bg = self.COLOR_MAIN,
                                   height = height, width = width)
        section = tk.Frame(master = pointMassConfig, bg = self.COLOR_MAIN)
        rvLabel = tk.Frame(master = pointMassConfig, bg = self.COLOR_MAIN)
        rvEntry = tk.Frame(master = pointMassConfig, bg = self.COLOR_MAIN)
        massLabel = tk.Frame(master = pointMassConfig, bg = self.COLOR_MAIN)
        massEntry = tk.Frame(master = pointMassConfig, bg = self.COLOR_MAIN)
        
        # Configure Rows and Columns
        pointMassConfig.rowconfigure(0, weight = 4)
        pointMassConfig.rowconfigure(1, weight = 1)
        pointMassConfig.rowconfigure(2, weight = 36)
        pointMassConfig.rowconfigure(3, weight = 6)
        pointMassConfig.rowconfigure(4, weight = 6)
        
        section.rowconfigure(0, weight = 1)
        section.columnconfigure(0, weight = 1)
        
        rvLabel.rowconfigure(0, weight = 1)
        rvLabel.columnconfigure(0, weight = 1)
        rvLabel.columnconfigure(1, weight = 1)
        
        for i in range(3):
            rvEntry.rowconfigure(i, weight = 1)
        
        
        # ----|Create GUI, Place and Add Items|----
        # Section Label and Toggle
        tk.Label(master = section,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                 font = (self.FONT_MAIN, self.FONT_SIZE_HEADER),
                 text = "Point Mass (HCI):").grid(row = 0, column = 0,
                                                  sticky = "W", padx = 1)
        
        toggle = ttk.Checkbutton(master = section, text = "Toggle",
                                 variable = self.insertPointMass,
                                 command = self.togglePointMass,
                                 style = "ss.TCheckbutton",
                                 takefocus = 0)
        toggle.grid(row = 0, column = 1, sticky = "W", padx = (3,0))
        self.gui[self.GUI_POINTMASS]["cbtn_togglePointMass"] = toggle
        
        
        # RV Labels
        tk.Label(master = rvLabel,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                 font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                 text = "Radius:").grid(row = 0, column = 0, sticky = "SW")
        tk.Label(master = rvLabel,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                 font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                 text = "Velocity:").grid(row = 0, column = 1, sticky = "SW",
                                          padx = (47,0))
        
        
        # RV Entry Field Loop
        rValidate = (self.window.register(self.checkRadius),
                     "%d", "%i", "%P", "%s", "%S")
        vValidate = (self.window.register(self.checkVelocity),
                     "%d", "%i", "%P", "%s", "%S")
        
        rvLabels = [["X:", "Y:", "Z:"],
                    ["VX:", "VY:", "VZ:"]]
        for i in range(3):
            # Labels
            tk.Label(master = rvEntry,
                     bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                     font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                     text = rvLabels[0][i]).grid(row = i, column = 0,
                                                 sticky = "NW")
            tk.Label(master = rvEntry,
                     bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                     font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                     text = "km").grid(row = i, column = 2, sticky = "NW")
            tk.Label(master = rvEntry,
                     bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                     font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                     text = rvLabels[1][i]).grid(row = i, column = 3,
                                                 sticky = "NW", padx = (4,0))
            tk.Label(master = rvEntry,
                     bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                     font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                     text = "km/s").grid(row = i, column = 5, sticky = "NW")
            
            # Entry Boxes
            rEntry = tk.Entry(master = rvEntry,
                              bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                              disabledbackground = self.COLOR_SUBTEXT,
                              textvariable = self.pointR[i],
                              validate = "key",
                              validatecommand = rValidate,
                              font = (self.FONT_MAIN,
                                      self.FONT_SIZE_MAIN),
                              width = 11)
            
            vEntry = tk.Entry(master = rvEntry,
                              bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                              disabledbackground = self.COLOR_SUBTEXT,
                              textvariable = self.pointV[i],
                              validate = "key",
                              validatecommand = vValidate,
                              font = (self.FONT_MAIN,
                                      self.FONT_SIZE_MAIN),
                              width = 4)
            
            # Disable
            rEntry.config(state = "disabled")
            vEntry.config(state = "disabled")
            
            # Place
            rEntry.grid(row = i, column = 1, sticky = "NW")
            vEntry.grid(row = i, column = 4, sticky = "NW")
            
            # Add to self.gui
            rKey = "".join(["txt_", "pointMassR", rvLabels[0][i][:-1]])
            vKey = "".join(["txt_", "pointMass", rvLabels[1][i][:-1]])
            self.gui[self.GUI_POINTMASS][rKey] = rEntry
            self.gui[self.GUI_POINTMASS][vKey] = vEntry
        
        
        # Mass Label
        tk.Label(master = massLabel,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                 font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                 text = "Mass:").grid(row = 0, column = 0, sticky = "NW")
        
        # Mass Entry
        tk.Label(master = massEntry,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAIN,
                 font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                 text = "X:").grid(row = 0, column = 0, sticky = "NW")
        
        mValidate = (self.window.register(self.checkPointMass),
                     "%d", "%i", "%P", "%s", "%S")
        mEntry = tk.Entry(master = massEntry,
                          bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                          disabledbackground = self.COLOR_SUBTEXT,
                          textvariable = self.pointM,
                          validate = "key",
                          validatecommand = mValidate,
                          font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                          width = 11)
        mEntry.config(state = "disabled")
        mEntry.grid(row = 0, column = 1, sticky = "NW")
        
        tk.Label(master = massEntry,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                 font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                 text = "kg").grid(row = 0, column = 2, sticky = "NW")
        
        self.gui[self.GUI_POINTMASS]["txt_pointMassM"] = mEntry
        
        
        # Place Frames
        section.grid(row = 0, column = 0, sticky = "NW")
        rvLabel.grid(row = 1, column = 0, sticky = "NSEW")
        rvEntry.grid(row = 2, column = 0, sticky = "NSEW")
        massLabel.grid(row = 3, column = 0, sticky = "NSEW")
        massEntry.grid(row = 4, column = 0, sticky = "NSEW")
        
        return pointMassConfig
    
    
    def planetConfigUI(self, row, col):
        """
        planetConfigUI returns a frame containing all the items needed to
        configure the planets in the simulation. The object's gui dictionary
        is modified to contain the ui elements added by this method.
        
        INPUT:
        row : int
            Integer defining the row within the main window this portion of
            the GUI will inhabit.
        col : int
            Integer defining the column within the main window this portion of
            the GUI will inhabit
        
        OUTPUT:
        planetConfig : tkinter Frame
            Tkinter Frame containing the planet configuration UI elements.
        """
        
        # ----|Create Dictionary|----
        self.gui[self.GUI_PLANETS] = dict()
        
        
        # ----|Add Traces to Input Vars|----
        self.numCustomPlanets.trace_add("write", self.checkCustomPlanets)
        
        
        # ----|Define Frames|----
        # Create
        height = int(self.WINDOW_HEIGHT*(self.ROWSIZES[row]/
                                         sum(self.ROWSIZES)))
        width = int(self.WINDOW_WIDTH*(self.COLSIZES[col]/
                                       sum(self.COLSIZES)))
        
        planetConfig = tk.Frame(master = self.window,
                                bg = self.COLOR_MAIN,
                                height = height, width = width)
        section = tk.Frame(master = planetConfig,
                         bg = self.COLOR_MAIN)
        boxes = tk.Frame(master = planetConfig,
                         bg = self.COLOR_MAIN)
        
        # Configure Rows and Columns
        planetConfig.rowconfigure(0, weight = 1)
        planetConfig.rowconfigure(1, weight = 4)
        planetConfig.columnconfigure(0, weight = 1)
        
        section.columnconfigure(0, weight = 1)
        section.rowconfigure(0, weight = 1)
        
        for i in range(3):
            boxes.rowconfigure(i, weight = 1)
            boxes.columnconfigure(i, weight = 1)
        boxes.columnconfigure(3, weight = 1)
        boxes.columnconfigure(4, weight = 1)
        
        
        # ----|Create, Place, and Add all GUI Items|----
        # Create and Place Section Label
        tk.Label(master = section,
                  bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                  font = (self.FONT_MAIN, self.FONT_SIZE_HEADER),
                  text = "Planets:").grid(row = 0, column = 0, sticky = "W")
        
        
        # Checkbuttons Loop
        for ind, planet in enumerate(self.PLANETS):
            # Create
            planetBox = ttk.Checkbutton(master = boxes, text = planet,
                                        variable = self.chosenPlanets[ind],
                                        style = "ss.TCheckbutton",
                                        takefocus = 0)
            
            # Place
            if ind != 9:
                # Normal Planet Checkbutton
                planetBox.grid(row = ind // 3, column = ind % 3, sticky = "W")
            else:
                # Custom Checkbutton
                planetBox.config(command = self.customPlanetCbtn)
                planetBox.grid(row = 1, column = 3, sticky = "W")
            
            # Add
            self.gui[self.GUI_PLANETS]["_".join(["cbtn", planet])] = planetBox
        
        
        # Custom Planet Number
        #  Create Frame, Label, and Combobox
        customPlanets = tk.Frame(master = boxes,
                                 bg = self.COLOR_MAIN)
        tk.Label(master = customPlanets,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                 font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                 text = "#").grid(row = 0, column = 0, padx = (2,1))
        numCustomPlanets = ttk.Combobox(master = customPlanets,
                                        textvariable = self.numCustomPlanets,
                                        values = [i for i in range(1,11)],
                                        font = (self.FONT_MAIN,
                                                self.FONT_SIZE_MAIN),
                                        style = "ss.TCombobox",
                                        width = 3)
        numCustomPlanets.config(state = "disabled")
        numCustomPlanets.grid(row = 0, column = 1)
        self.gui[self.GUI_PLANETS]["menu_numCustomPlanets"] = numCustomPlanets
        
        
        # Place Frames
        customPlanets.grid(row = 1, column = 4, sticky = "NSEW")
        section.grid(row = 0, column = 0, sticky = "NW")
        boxes.grid(row = 1, column = 0, sticky = "NW", padx = (2,0))
        
        return planetConfig
    
    
    def customPlanetConfigUI(self, row, col):
        """
        customPlanetConfigUI returns a frame containing all the items needed
        to configure the custom planet(s) in the simulation. The object's gui
        dictionary is modified to contain the ui elements added by
        this method.
        
        INPUT:
        row : int
            Integer defining the row within the main window this portion of
            the GUI will inhabit.
        col : int
            Integer defining the column within the main window this portion of
            the GUI will inhabit

        OUTPUT:
        customPlanetConfig : tkinter Frame
            Tkinter Frame containing the custom planet configuration UI
            elements.
        """
        
        # ----|Create Dictionary|----
        self.gui[self.GUI_CUSTOM] = dict()
        
        
        # ----|Add Traces to Input Vars|----
        self.curCustomPlanet.trace_add("write", self.checkCurCustomPlanet)
        
        
        # ----|Define Frames|----
        # Create
        height = int(self.WINDOW_HEIGHT*(self.ROWSIZES[row]/
                                         sum(self.ROWSIZES)))
        width = int(self.WINDOW_WIDTH*(self.COLSIZES[col]/
                                       sum(self.COLSIZES)))
        
        customPlanetConfig = tk.Frame(master = self.window,
                                      bg = self.COLOR_MAIN,
                                      height = height, width = width)
        section = tk.Frame(master = customPlanetConfig,
                           bg = self.COLOR_MAIN)
        rvLabel = tk.Frame(master = customPlanetConfig,
                           bg = self.COLOR_MAIN)
        rvEntry = tk.Frame(master = customPlanetConfig,
                           bg = self.COLOR_MAIN)
        propertiesLabel = tk.Frame(master = customPlanetConfig,
                                   bg = self.COLOR_MAIN)
        propertiesEntry = tk.Frame(master = customPlanetConfig,
                                   bg = self.COLOR_MAIN)
        
        # Configure Rows and Columns
        customPlanetConfig.rowconfigure(0, weight = 4)
        customPlanetConfig.rowconfigure(1, weight = 1)
        customPlanetConfig.rowconfigure(2, weight = 36)
        customPlanetConfig.rowconfigure(3, weight = 6)
        customPlanetConfig.rowconfigure(4, weight = 6)
        
        section.rowconfigure(0, weight = 1)
        section.columnconfigure(0, weight = 1)
        
        rvLabel.rowconfigure(0, weight = 1)
        rvLabel.columnconfigure(0, weight = 1)
        rvLabel.columnconfigure(1, weight = 1)
        
        for i in range(3):
            rvEntry.rowconfigure(i, weight = 1)
        
        propertiesLabel.columnconfigure(0, weight = 1)
        propertiesLabel.columnconfigure(1, weight = 1)
        
        
        # ----|Create GUI, Place and Add Items|----
        split = 30
        
        # Section Label and Custom Planet Number
        tk.Label(master = section,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                 font = (self.FONT_MAIN, self.FONT_SIZE_HEADER),
                 text = "Custom Planets (HCI):").grid(row = 0, column = 0,
                                                      sticky = "W", padx = 1)
        
        customPlanetNum = ttk.Combobox(master = section,
                                       textvariable = self.curCustomPlanet,
                                       values = [0],
                                       font = (self.FONT_MAIN,
                                               self.FONT_SIZE_MAIN),
                                       style = "ss.TCombobox",
                                       width = 3)
        customPlanetNum.grid(row = 0, column = 1, sticky = "W", padx = (4,0))
        self.gui[self.GUI_CUSTOM]["menu_customPlanetNum"] = customPlanetNum
        
        
        # RV Labels
        tk.Label(master = rvLabel,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                 font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                 text = "Radius:").grid(row = 0, column = 0, sticky = "SW")
        tk.Label(master = rvLabel,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                 font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                 text = "Velocity:").grid(row = 0, column = 1, sticky = "SW",
                                          padx = (split + 5,0))
        
        
        # RV Entry Field Loop
        rValidate = (self.window.register(self.checkRadius),
                     "%d", "%i", "%P", "%s", "%S")
        vValidate = (self.window.register(self.checkVelocity),
                     "%d", "%i", "%P", "%s", "%S")
        
        rvLabels = [[ "X",  "Y",  "Z"],
                    ["VX", "VY", "VZ"]]
        for i in range(3):
            # Labels
            tk.Label(master = rvEntry,
                     bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                     font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                     text = rvLabels[0][i] + ":").grid(row = i, column = 0,
                                                       sticky = "NW")
            tk.Label(master = rvEntry,
                     bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                     font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                     text = "km").grid(row = i, column = 2, sticky = "NW")
            tk.Label(master = rvEntry,
                     bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                     font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                     text = rvLabels[1][i] + ":").grid(row = i, column = 3,
                                                       sticky = "NW",
                                                       padx = (split + 4,0))
            tk.Label(master = rvEntry,
                     bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                     font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                     text = "km/s").grid(row = i, column = 5, sticky = "NW")
            
            # Entry Boxes
            rEntry = tk.Entry(master = rvEntry,
                              bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                              disabledbackground = self.COLOR_SUBTEXT,
                              # textvariable = self.pointR[i],
                              validate = "key",
                              validatecommand = rValidate,
                              font = (self.FONT_MAIN,
                                      self.FONT_SIZE_MAIN),
                              width = 11)
            
            vEntry = tk.Entry(master = rvEntry,
                              bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                              disabledbackground = self.COLOR_SUBTEXT,
                              # textvariable = self.pointV[i],
                              validate = "key",
                              validatecommand = vValidate,
                              font = (self.FONT_MAIN,
                                      self.FONT_SIZE_MAIN),
                              width = 4)
            
            # Place
            rEntry.grid(row = i, column = 1, sticky = "NW")
            vEntry.grid(row = i, column = 4, sticky = "NW")
            
            # Add to self.gui
            self.gui[self.GUI_CUSTOM]["".join(["txt_", "planetR",
                                               rvLabels[0][i]])] = rEntry
            self.gui[self.GUI_CUSTOM]["".join(["txt_", "planet",
                                               rvLabels[1][i]])] = vEntry
        
        
        # Properties Labels
        tk.Label(master = propertiesLabel,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                 font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                 text = "Mass:").grid(row = 0, column = 0, sticky = "NW")
        tk.Label(master = propertiesLabel,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                 font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                 text = "Radius:").grid(row = 0, column = 1,
                                        sticky = "NW", padx = (split + 5,0))
        
        # Properties Entry
        #  Mass
        tk.Label(master = propertiesEntry,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAIN,
                 font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                 text = "X:").grid(row = 0, column = 0, sticky = "NW")
        
        mValidate = (self.window.register(self.checkPlanetaryMass),
                     "%d", "%i", "%P", "%s", "%S")
        pMassVar = self.customPlanetProperties[0]["mass"]
        pmEntry = tk.Entry(master = propertiesEntry,
                           bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                           disabledbackground = self.COLOR_SUBTEXT,
                           textvariable = pMassVar,
                           validate = "key",
                           validatecommand = mValidate,
                           font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                           width = 11)
        pmEntry.grid(row = 0, column = 1, sticky = "NW")
        
        tk.Label(master = propertiesEntry,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                 font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                 text = "E24 kg").grid(row = 0, column = 2, sticky = "NW")
        
        #  Radius
        tk.Label(master = propertiesEntry,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAIN,
                 font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                 text = "VX:").grid(row = 0, column = 3,
                                    sticky = "NW", padx = (split - 21,0))
        
        rValidate = (self.window.register(self.checkPlanetaryRadius),
                     "%d", "%i", "%P", "%s", "%S")
        pRadVar = self.customPlanetProperties[0]["radius"]
        prEntry = tk.Entry(master = propertiesEntry,
                           bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                           disabledbackground = self.COLOR_SUBTEXT,
                           textvariable = pRadVar,
                           validate = "key",
                           validatecommand = mValidate,
                           font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                           width = 11)
        prEntry.grid(row = 0, column = 4, sticky = "NW")
        
        tk.Label(master = propertiesEntry,
                 bg = self.COLOR_MAIN, fg = self.COLOR_MAINTEXT,
                 font = (self.FONT_MAIN, self.FONT_SIZE_MAIN),
                 text = "km").grid(row = 0, column = 5, sticky = "NW")
        
        #  Place in dictionary
        self.gui[self.GUI_CUSTOM]["txt_planetM"] = pmEntry
        self.gui[self.GUI_CUSTOM]["txt_planetR"] = prEntry
        
        
        # Disable All
        for key, item in self.gui[self.GUI_CUSTOM].items():
            item.config(state = "disabled")
        
        
        # Place Frames
        section.grid(row = 0, column = 0, sticky = "NW")
        rvLabel.grid(row = 1, column = 0, sticky = "NSEW")
        rvEntry.grid(row = 2, column = 0, sticky = "NSEW")
        propertiesLabel.grid(row = 3, column = 0, sticky = "NSEW")
        propertiesEntry.grid(row = 4, column = 0, sticky = "NSEW")
        
        return customPlanetConfig
    
    
    def startResetUI(self, row, col):
        """
        startRestUI returns a frame containig the two buttons for starting and
        resetting the simulation. The object's gui dictionary is modified to
        contain the ui elements added by this method.
        
        INPUT:
        row : int
            Integer defining the row within the main window this portion of
            the GUI will inhabit.
        col : int
            Integer defining the column within the main window this portion of
            the GUI will inhabit

        OUTPUT:
        startReset : tkinter Frame
            Tkinter Frame containing the start and reset simulation buttons.
        """
        
        # ----|Define Frame|----
        height = int(self.WINDOW_HEIGHT*(self.ROWSIZES[row]/
                                         sum(self.ROWSIZES)))
        width = int(self.WINDOW_WIDTH*(self.COLSIZES[col]/
                                       sum(self.COLSIZES)))
        
        startReset = tk.Frame(master = self.window, bg = self.COLOR_MAIN,
                              height = height, width = width)
        
        
        # ----|Create Buttons|----
        start = ttk.Button(master = startReset, command = self.startSim,
                           text = "Start",
                           width = 9, style = "ss.TButton")
        reset = ttk.Button(master = startReset, command = self.resetSim,
                           text = "Reset",
                           width = 7, style = "ss.TButton")
        
        
        # ----|Place Buttons|----
        startReset.rowconfigure(0, weight = 1)
        startReset.grid_anchor("se")
        reset.grid(row = 0, column = 0, sticky = "SE", pady = 5)
        start.grid(row = 0, column = 1, sticky = "SE", padx = 5, pady = 5)
        
        
        # ----|Add Buttons to self.gui|----
        self.gui[self.GUI_STARTRESET] = {"btn_start": start,
                                         "btn_reset": reset}
        
        return startReset
    # --------
    
    
    # ----|Functionality Methods|----
    def checkValidConfig(self):
        """
        checkValidConfig is called when the start button is pressed to
        determine if the currently input set is valid for a simulation.
        
        OUTPUT:
        message : string
            A potential error message to be displayed according to the state
            of the configuration. Returns an empty string if no errors occur.
        """
        
        message = ""
        messages = {"Time": "Time Configuration is Incomplete!",
                    "Custom": "Custom Planet Configuration is Incomplete!",
                    "Point Mass": "Point Mass Configuration is Incomplete!",
                    "Nothing": "Nothing to Simulate!"}
        valid = True
        
        # Check Time Config
        if not self.validSimTimes:
            message += messages["Time"]
        
        # Check Planet Config
        #  The user does not need to select any planets, only need to ensure
        #  that if the user has selected Custom Planets that they have
        #  entered all the information necessary.
        if self.chosenPlanets[-1].get():
            # Check all custom planets that are defined
            try:
                for planet in range(int(self.numCustomPlanets.get())):
                    planetDict = self.customPlanetProperties[planet]
                    
                    for key, item in planetDict.items():
                        if key == "mass" or key == "radius":
                            if not planetDict[key].get():
                                if message:
                                    message += "\n"
                                message += messages["Custom"]
                                valid = False
                                break
                        else:
                            for val in item:
                                if not val.get():
                                    if message:
                                        message += "\n"
                                    message += messages["Custom"]
                                    valid = False
                                    break
                        
                        if not valid:
                            break
                    if not valid:
                        break
            except ValueError:
                if message:
                    message += "\n"
                message += messages["Custom"]
        
        # Check Point Mass
        if self.insertPointMass.get():
            valid = True
            
            for item in self.inputVars[self.GUI_POINTMASS]:
                if type(item) == type([]):
                    for val in item:
                        if not val.get():
                            if message:
                                message += "\n"
                            message += messages["Point Mass"]
                            valid = False
                            break
                
                else:
                    if not item.get():
                        if message:
                            message += "\n"
                        message += messages["Point Mass"]
                        valid = False
                        break
                
                if not valid:
                    break
        
        # # User must have selected something outside of a time
        if ((not sum([planet.get() for planet in self.chosenPlanets]))
                and (not self.insertPointMass.get())):
            message = messages["Nothing"]
        
        # Return message, will be empty if all entries are valid
        return message
    
    
    def startSim(self):
        """
        startSim handles starting the simulation once all inputs have been
        satisfied
        """
        
        # ----|Check for valid simulation configuration|----
        errorMsg = self.checkValidConfig()
        
        if errorMsg:
            tk.messagebox.showerror(title = "Invalid Sim Configuration",
                                    message = errorMsg,
                                    parent = self.window)
            return
        
        
        # ----|Valid Sim Setup|----
        # Grab current time
        curTime = datetime.now()
        
        # Create sim system
        start = self.getStartTime()
        system = self.setupSystem(curTime, start.strftime(self.TIMEFORMAT))
        
        # Setup time steps
        simLen = float(self.simLength.get())
        numSteps = int(simLen*24) # Update position ~once an hour
        timeStep = timedelta(days = simLen/numSteps) # timeStep is in Days
        timeSteps = [(start + i*timeStep) for i in range(numSteps + 1)]
        
        
        # ----|Start Simulation|----
        results = self.runSim(system, start, timeSteps)
        
        # ----|Graph Results|----
        Graph(results, start)
        
        # ----|Print to File|----
        self.printToFile(results, start)
    
    
    def resetSim(self):
        """
        resetSim restarts the Solarsim instance to simulate again.
        """
        
        # Clear Variables
        listType = type([])
        stringType = type(tk.StringVar())
        intType = type(tk.IntVar())
        
        for key, section in self.inputVars.items():
            for var in section:
                # Determine var type
                varType = type(var)
                
                # StringVar
                if varType == stringType:
                    var.set("")
                
                # IntVar
                elif varType == intType:
                    var.set(0)
                
                # List
                elif varType == listType:
                    for val in var:
                        valType = type(val)
                        
                        # StringVar
                        if valType == stringType:
                            val.set("")
                        
                        # IntVar
                        elif valType == intType:
                            val.set(0)
        
        
        # Clear self.customPlanetProperties
        for planetDict in self.customPlanetProperties:
            for key, item in planetDict.items():
                if key == "mass" or key == "radius":
                    item.set("")
                else:
                    for val in item:
                        val.set("")
        
        
        # Disable GUI Items
        for key, section in self.defaultDisabled.items():
            for item in section:
                self.gui[key][item].config(state = "disabled")
    # --------
    
    
    # ----|Simulation Functions|----
    def getStartTime(self):
        """
        getStartTime is a helper function to runSim. Returns the start time
        from user input.
        
        OUTPUT:
        start : datetime
            Datetime object containing the user input start time.
        """
        
        start = datetime(int(self.startYear.get()),
                         self.MONTHS.index(self.startMonth.get()) + 1,
                         int(self.startDay.get()),
                         int(self.startHour.get()),
                         int(self.startMinute.get()),
                         int(self.startSeconds.get()))
        
        return start
    
    
    def setupSystem(self, curTime, start):
        """
        setupSystem is a helper method to runSim that handles reading the
        user inputs and setting up the system dictionary.
        
        OUTPUT:
        system : dictionary
            Dictionary containing the central star, the planets, 
            and point mass. System takes on the the following format:
                system = {"star": spaceObj.Star Object,
                          "planets": [spaceObj.Planet Object 1,
                                      spaceObj.Planet Object 2,
                                          .
                                          .
                                          .
                                      spaceObj.Planet Object N],
                          "point": spaceObj.PointMass Object}
        """
        
        # Define system dictionary
        system = dict()
        
        # Add Sun
        system["star"] = so.Star("Sun")
        
        
        # ----|Read Inputs|----
        # Chosen Planets
        system["planets"] = []
        
        bodies = [self.PLANETS[i]
                  for i in range(len(self.PLANETS[:-1]))
                  if self.chosenPlanets[i].get()]
        numBodies = len(bodies)
        
        # Point Mass
        if self.insertPointMass.get():
            pointProp = {"mass": float(self.pointM.get()),
                         "center": np.array([float(r.get())*1E3
                                             for r in self.pointR]),
                         "velocity": np.array([float(v.get())*1E3
                                               for v in self.pointV])}
            system["point"] = so.PointMass("-".join(["PM",
                                                     curTime.strftime("%f")]),
                                           pointProp)
        
        # Custom Planets
        if self.chosenPlanets[-1].get():
            custPlanets = []
            for i in range(int(self.numCustomPlanets.get())):
                # Get and Format current planet properties
                curPlanet = self.customPlanetProperties[i]
                prop = {"mass": float(curPlanet["mass"].get())*1E24,
                        "radius": float(curPlanet["radius"].get())*1E3,
                        "center": np.array([(float(r.get()))*1E3
                                            for r in curPlanet["center"]]),
                        "velocity": np.array([(float(v.get()))*1E3
                                              for v in curPlanet["velocity"]])}
                
                # Create Planet Object
                custPlanets.append(so.Planet(" ".join(["Custom", str(i + 1)]),
                                             prop))
        
        
        # ----|Get Ephmerides|----
        # Create file name
        timeString = curTime.strftime("%Y-%b-%d-%H-%M-%S")
        filename = "".join([timeString, "-", str(numBodies),
                            ["body", "bodies"][numBodies > 1],
                            ".txt"])
        
        # Pull Data
        eph.getEph(bodies, start, filename)
        
        # Create Dictionary
        ephemeris = eph.createEph(filename)
        
        
        # ----|Create and Store Planets|----
        system["planets"] = [so.Planet(body, ephemeris[body])
                             for body in bodies]
        
        if self.chosenPlanets[-1].get():
            system["planets"] += custPlanets
        
        
        return system
    
    
    def runSim(self, system, start, timeSteps):
        """
        runSim handles running all simulation steps and returns the results
        of the sim.
        
        INPUT:
        system : dictionary
            Dictionary containing the star, planets, and point mass within the
            system. System takes on the the following format:
                system = {"star": spaceObj.Star Object,
                          "planets": [spaceObj.Planet Object 1,
                                      spaceObj.Planet Object 2,
                                          .
                                          .
                                          .
                                      spaceObj.Planet Object N],
                          "point": spaceObj.PointMass Object}
        start : string
            String defining the starting time of the simulation in the
            following format:
                YYYY-MMM-DD hh:mm:ss.ffff
        timeSteps : list
            List of datetime objects containing all evaluation times in the
            simulation. The first datetime object is the start time input by
            the user, and thus the results to do not need to be calculated
            for this time.
        
        OUTPUT:
        results : dictionary
            2D dictionary containing the positions at each time step for each
            body input by the user. Format is as follows:
                results = {"star": np.zeros(3),
                           "planets": {"planet1": np.array((numSteps, 3)),
                                       "planet2": np.array((numsSteps, 3)),
                                           .
                                           .
                                           .
                                       "planetN": np.array((numsSteps, 3))},
                           "point": np.array((numsSteps, 3))}
        """
        
        # ----|Create results dictionary|----
        numSteps = len(timeSteps)
        results = dict()
        
        # Star
        results["star"] = np.zeros(3)
        
        # Planets
        if "planets" in system.keys():
            # Combine initial position with zeros array after
            results["planets"] = {p.name:np.append([p.rCurr],
                                                   np.zeros((numSteps - 1,3)),
                                                   axis = 0)
                                  for p in system["planets"]}
        
        # Point Mass
        if "point" in system.keys():
            # Combine initial position with zeros array after
            results["point"] = np.append([system["point"].rCurr],
                                         np.zeros((numSteps - 1,3)),
                                         axis = 0)
        
        # ----|Call Predictor-Corrector Loop|----
        pr.predCorr(system, timeSteps, self.tol, results)
        
        return results
    
    
    def printToFile(self, results, start):
        """
        printToFile prints the results of the simulation to a CSV file.

        INPUT:
        results : dictionary
            2D dictionary containing the positions at each time step for each
            body input by the user. Format is as follows:
                results = {"star": np.zeros(3),
                           "planets": {"planet1": np.array((numSteps, 3)),
                                       "planet2": np.array((numsSteps, 3)),
                                           .
                                           .
                                           .
                                       "planetN": np.array((numsSteps, 3))},
                           "point": np.array((numsSteps, 3))}
        start : datetime
            Datetime object containing the user input start time.

        OUTPUT:
        file
            Writes a CSV file containing the reuslts of the simulation.
            File is written to the /results/ subdirectory of the project.
        """
        
        # Create filename
        filename = "".join([RESULTSDIR,"results_",
                            start.strftime("%Y-%b-%d-%H-%M-%S"), ".csv"])
        
        # Check if exists and modify
        if exists(filename):
            count = 1
            filename = "".join([filename[:-4], "_1.csv"])
            
            # Keep incrementing until unique
            while exists(filename):
                digits = len(str(count))
                index = -4 - digits
                count += 1
                
                filename = "".join([filename[:index], "_",
                                    str(count), ".csv"])
        
        with open(filename, 'w') as file:
            # Star
            file.write("".join(["Star\n",
                                str(results["star"][0]), ",",
                                str(results["star"][1]), ",",
                                str(results["star"][2]), "\n\n"]))
            
            # Planets
            if "planets" in results.keys():
                planets = list(results["planets"].keys())
                
                # Write Planets labels
                file.write(",,,,".join(planets) + "\n")
                
                # Write Planet positions
                for t in range(len(results["planets"][planets[0]])):
                    s = (",,".join([",".join([str(results["planets"][p][t][k])
                                              for k in range(3)])
                                    for p in planets]) + "\n")
                    file.write(s)
                file.write("\n\n")
            
            
            # Point Mass
            if "point" in results.keys():
                file.write("Point Mass\n")
                
                for pos in results["point"]:
                    file.write("".join([str(pos[0]), ",",
                                        str(pos[1]), ",",
                                        str(pos[2]), ",",
                                        "\n"]))
    # --------
    
    
    # ----|Trace, Validation, and Widget Functions|----
    # --------|Helper Functions|--------
    # Time Config
    def disableAllTimeEntry(self):
        """
        Disables all date entry fields if the year becomes invalid.
        """
        
        for key, item in self.gui[self.GUI_TIME].items():
            if key != "txt_startYear":
                item.config(state = "disabled")
    
    
    # Value Checking
    def checkValPos(self, maxVal, *args):
        """
        checkValPos is a helper function to the data entry field validation
        functions. This function only allows positive numbers to be entered.
        
        INPUT:
            maxVal : int or float
                maxVal contains the maximum allowable value for the
                input field
            *args : tuple
                tuple containing all arguments passed to the
                validation function. In the following order:
                    (%d, %i, %P", %s, %S)
        
        OUTPUT:
            Boolean
                Validity of the input
        """
        
        # Check if a value has been entered
        if args[2]:
            try:
                # Try to convert to a float
                val = float(args[2])
                
                # Don't allow 0 as the first entry
                if args[2] == "0":
                    return False
                
                # Don't allow value to exceed maxVal
                if val > maxVal:
                    return False
                
                # Valid Entry
                return True
            
            except ValueError:
                # Allow entry of decimals
                if args[2] == ".":
                    return True
                return False
        
        # Allow user to clear the entry field
        return True
    
    
    def checkValNeg(self, maxVal, *args):
        """
        checkValPos is a helper function to the data entry field validation
        functions. This function allows negative and positive numbers to be
        entered.
        
        INPUT:
            maxVal : int or float
                maxVal contains the maximum allowable value for the
                input field
            *args : tuple
                tuple containing all arguments passed to the
                validation function. In the following order:
                    (%d, %i, %P", %s, %S)
        
        OUTPUT:
            Boolean
                Validity of the input
        """
        
        # Check if a value has been entered
        if args[2]:
            try:
                # Try to convert to a float
                val = float(args[2])
                
                # Don't allow 0 as the first entry
                if args[2] == "0":
                    return False
                
                # Don't allow value to exceed maxVal
                if abs(val) > maxVal:
                    return False
                
                # Valid Entry
                return True
            
            except ValueError:
                # Allow entry of negative values, decimals, and
                # negative decimals.
                if args[2] in ["-", ".", "-."]:
                    return True
                return False
        
        # Allow user to clear the entry field
        return True
    # ----------------
    
    
    # --------|Time Config|--------
    def checkYear(self, *args):
        """
        checkYear is the validation function for the user's input year.
        Only allows the user to input a 4 digit integer year. Sets the text
        color to red if the year is before 1900, and black if equal to or
        after.
        """
        
        # Grab UI elements
        monthDrop = self.gui[self.GUI_TIME]["menu_startMonth"]
        yearEntry = self.gui[self.GUI_TIME]["txt_startYear"]
        
        # Check if a year has been entered and is 4 digits or less
        if args[2] and (len(args[2]) <= 4):
            try:
                # Try to convert to an int
                year = int(args[2])
                
                if year < 1900:
                    yearEntry.config(fg = "red")
                else:
                    yearEntry.config(fg = self.COLOR_MAINTEXT)
                
                # Don't allow 0 as the first entry
                if args[2] == "0":
                    return False
                
                # Enable the month dropdown menu, entered year is valid
                self.checkMonth("", "", "", args[2])
                monthDrop.config(state = "readonly")
                return True
            
            except ValueError:
                # User attempted to enter a non digit
                return False
        
        # Don't allow entry of more than 4 digits
        elif args[2] and len(args[2]) > 4:
            return False
        
        # Allow user to clear the entry field
        self.disableAllTimeEntry()
        return True
    
    
    def checkMonth(self, *args):
        """
        checkMonth is the trace function called when the user changes the
        input month. When the input month is changed, the available days to
        choose from are modified according to the number of days in that month
        for that year.
        """
        
        # Define variables
        dayDrop = self.gui[self.GUI_TIME]["menu_startDay"]
        month = self.startMonth.get()
        if len(args) == 4:
            year = int(args[3])
        elif self.startYear.get():
            year = int(self.startYear.get())
        else:
            return
        
        if month in self.MONTHS:
            # Update the days menu entries
            monthIndex = self.MONTHS.index(month)
            isLeap = isleap(year)
            
            # Check for February of Leap Year
            if isLeap and monthIndex == 1:
                days = [i for i in range(1, self.DAYS[-1] + 1)]
            else:
                days = [i for i in range(1, self.DAYS[monthIndex] + 1)]
            
            # Modify menu contents
            dayDrop["values"] = days
            
            # Enable dropdown menu
            dayDrop.config(state = "readonly")
        else:
            dayDrop.config(state = "disabled")
    
    
    def checkDay(self, *args):
        """
        checkDay is the trace function called when the user changes the
        input day. Enables the user to select the input hour once a day has
        been input.
        """
        
        hourMenu = self.gui[self.GUI_TIME]["menu_startHour"]
        if str(hourMenu["state"]).lower() == "disabled":
            hourMenu.config(state = "readonly")
    
    
    def checkHour(self, *args):
        """
        checkHour is the trace function called when the user changes the
        input hour. Enables the user to select the input minute once an hour
        has been input.
        """
        
        minuteMenu = self.gui[self.GUI_TIME]["menu_startMinute"]
        if str(minuteMenu["state"]).lower() == "disabled":
            minuteMenu.config(state = "readonly")
    
    
    def checkMinute(self, *args):
        """
        checkMinute is the trace function called when the user changes the
        input minute. Enables the user to select the input second once a
        minute has been input.
        """
        
        secondMenu = self.gui[self.GUI_TIME]["menu_startSecond"]
        if str(secondMenu["state"]).lower() == "disabled":
            secondMenu.config(state = "readonly")
    
    
    def checkSeconds(self, *args):
        """
        checkSeconds is the trace function called when the user changes the
        input seconds. Enables the user to select the input sim length once a
        second has been input.
        """
        
        simLenEntry = self.gui[self.GUI_TIME]["txt_simLength"]
        if str(simLenEntry["state"]).lower() == "disabled":
            simLenEntry.config(state = "normal")
    
    
    def checkSimLen(self, *args):
        """
        checkSimLen is the validation function for the user's input
        Sim Length. Only allows the user to input a float defining the number
        of days to simulate.
        """
        
        # Don't allow more than approximately 50 years in days
        valid = self.checkValPos(20000, *args)
        
        # Check if sim length is valid
        if args[2] and valid and int(self.startYear.get()) >= 1900:
            self.validSimTimes = True
        else:
            self.validSimTimes = False
        
        return valid
    # ----------------
    
    
    # --------|Planet Config|--------
    def customPlanetCbtn(self):
        """
        customPlanetCbtn is the Checkbutton command function for the
        Custom Planet Checkbutton in the Planet Config GUI. This function
        enables or disables the Combobox containing the option for the number
        of Custom Planets the user can input.
        """
        
        # Get Combobox
        cbx = self.gui[self.GUI_PLANETS]["menu_numCustomPlanets"]
        
        # Enable or Disable
        if self.chosenPlanets[-1].get():
            cbx.config(state = "readonly")
        else:
            cbx.config(state = "disabled")
        
        # Enable or Disable the Custom Planet GUI
        if self.numCustomPlanets.get():
            self.enableCustomPlanetGUI()
    
    
    def checkCustomPlanets(self, *args):
        """
        checkCustomPlanets is the trace function attached to the number
        of Custom Planets variable. This function will enable the
        Custom Planet Config GUI once the number of custom planets and the
        current custom planet has been set, as well as set the values of the
        custom planet selection Combobox
        """
        
        # Grab Combobox and Entry Box
        cbx = self.gui[self.GUI_CUSTOM]["menu_customPlanetNum"]
        entry = self.gui[self.GUI_CUSTOM]["txt_planetRX"]
        
        # Enable the Custom Planet Combobox if necessary
        if str(cbx["state"]) == "disabled":
            cbx.config(state = "readonly")
        
        try:
            # Set the number of configurable planets Combobox values
            numCustomPlanets = int(self.numCustomPlanets.get())
            cbx.config(values = [i for i in range(1, numCustomPlanets + 1)])
            
            # Set the current custom planet value if it exceeds the selected
            # number of custom planets
            if int(self.curCustomPlanet.get()) > numCustomPlanets:
                self.curCustomPlanet.set(self.numCustomPlanets.get())
            
            # Enable the Custom Planet GUI if necessary
            if entry["state"] == "disabled":
                self.enableCustomPlanetGUI()
        except ValueError:
            pass
    # ----------------
    
    
    # --------|Point Mass and Custom Planet Config|--------
    def togglePointMass(self):
        """
        togglePointMass enables and disables the Point Mass entry GUI
        according to the state of self.insertPointMass.
        """
        
        # Grab UI Items
        pointMassDict = self.gui[self.GUI_POINTMASS]
        
        
        # Determine Toggle
        state = "disabled"
        if self.insertPointMass.get():
            state = "normal"
        
        for key, item in pointMassDict.items():
            # Only disable text entry
            if key[0] == "t":
                item.config(state = state)
    
    
    def checkRadius(self, *args):
        """
        checkRadius is the validation function for the Point Mass and
        Custom Planet radius entry fields.
        """
        
        # Don't allow a radius greater than approximately twice the
        # the semi-major axis of Pluto's orbit
        return self.checkValNeg(6000E6, *args)
    
    
    def checkVelocity(self, *args):
        """
        checkVelocity is the validation function for the Point Mass and
        Custom Planet velocity entry fields.
        """
        
        # Don't allow velocity greater than the approximate velocity of the
        # Parker Solar Probe at it's maximum velocity
        return self.checkValNeg(200, *args) # E3 km/s
    
    
    def checkPointMass(self, *args):
        """
        checkPointMass is the validation function for the Point Mass mass
        entry field.
        """
        
        # Don't allow mass greater than the approximate weight of a full
        # Saturn V
        return self.checkValPos(3000000, *args) # kg
    
    
    def enableCustomPlanetGUI(self):
        """
        enableCustomPlanetGUI enables or disables the state of the Custom
        Planet GUI section based upon the user's choice to incorporate them
        or not.
        """
        
        # Enable all elements
        if self.chosenPlanets[-1].get():
            for key, item in self.gui[self.GUI_CUSTOM].items():
                if key[:4] == "menu":
                    item.config(state = "readonly")
                else:
                    item.config(state = "normal")
        else:
            for key, item in self.gui[self.GUI_CUSTOM].items():
                item.config(state = "disabled")
    
    
    def checkCurCustomPlanet(self, *args):
        """
        checkCurCustomPlanet is the trace function for the curCustomPlanet
        variable. This function handles changing the Custom Planet GUI to
        display the currently selected custom planet values.
        """
        
        # Check for disabled GUI
        if self.gui[self.GUI_CUSTOM]["txt_planetRX"]["state"] == "disabled":
            self.enableCustomPlanetGUI()
        
        # Get planet number and dict
        try:
            planet = int(self.curCustomPlanet.get()) - 1
        except ValueError:
            planet = 0
        planetDict = self.customPlanetProperties[planet]
        
        # Modify Entry Boxes
        let2num = {"X":0, "Y":1, "Z":2}
        
        for key, item in self.gui[self.GUI_CUSTOM].items():
            # Only modify entry fields
            if key[:3] == "txt":
                # Center and Velocity
                if key[-1] in let2num.keys():
                    # Velocity
                    if key[-2] == "V":
                        var = planetDict["velocity"][let2num[key[-1]]]
                    
                    # Center
                    else:
                        var = planetDict["center"][let2num[key[-1]]]
                
                # Mass
                elif key[-1] == "M":
                    var = planetDict["mass"]
                
                # Planet Radius
                else:
                    var = planetDict["radius"]
            
                # Configure Entry Field
                item.config(textvariable = var)
    
    
    def checkPlanetaryRadius(self, *args):
        """
        checkPlanetaryRadius is the validation function for the
        Planetary Radius entry field.
        """
        
        # Don't allow more than approximately 3 times the radius of Jupiter
        # Largest confirmed exoplanet is 2.5 Jupiter Radii
        return self.checkValPos(250000, *args) # km
    
    
    def checkPlanetaryMass(self, *args):
        """
        checkPlanetaryRadius is the validation function for the
        Planetary Radius entry field.
        """
        
        return self.checkValPos(20000, *args) # E24 kg
    # --------
    
    
    def close(self):
        """
        Handles the closure of the Solarsim window.
        """
        
        self.window.quit()
        self.window.destroy()
# ----------------------------------------------------------------------------


# --|Graph Class|-------------------------------------------------------------
class Graph(Solarsim):
    """
    The Graph class handles plotting the results of a simulation using
    Matplotlib, and displaying the resulting graph in a separate Tkinter
    window.
    """
    
    # ----|CLASS VARIABLES|---------------------------------------------------
    KEYS = (Solarsim.PLANETS[:-1] +
            [("Custom " + str(i + 1)) for i in range(10)] +
            ["Point Mass"])
    COLORS = ["#DF762B", # Mercury
              "#E3AD2D", # Venus
              "#74C47C", # Earth
              "#B14825", # Mars
              "#B29A7B", # Jupiter
              "#FFBD00", # Saturn
              "#7DD7E6", # Uranus
              "#3652F3", # Neptune
              "#8E83B0", # Pluto
              "#CC2929", # Custom 1
              "#FF8700", # Custom 2
              "#FFD300", # Custom 3
              "#B6CC29", # Custom 4
              "#8DCC29", # Custom 5
              "#29CC88", # Custom 6
              "#29C1CC", # Custom 7
              "#2975CC", # Custom 8
              "#5D29CC", # Custom 9
              "#A129CC", # Custom 10
              "#888888"] # Point Mass
    
    
    # ----|Methods|-----------------------------------------------------------
    def __init__(self, results, start):
        """
        Initialize a new Graph object with the results of a simulation.

        INPUT:
        results : dictionary
            2D dictionary containing the positions at each time step for each
            body input by the user. Format is as follows:
                results = {"star": np.zeros(3),
                           "planets": {"planet1": np.array((numsSteps, 3)),
                                       "planet2": np.array((numsSteps, 3)),
                                           .
                                           .
                                           .
                                       "planetN": np.array((numsSteps, 3))},
                           "point": np.array((numsSteps, 3))}
        """
        
        # ----|Get simulation information|----
        title = start.strftime(Solarsim.TIMEFORMAT) + " - "
        
        # Planets
        planets = "planets" in results.keys()
        numPlanets = 0
        if planets:
            numPlanets = len(results["planets"])
            title += (str(numPlanets) + " " +
                      ["Planet", "Planets"][numPlanets > 1])
        
        # Point Mass
        pointMass = "point" in results.keys()
        if pointMass and planets:
            title += " and Point Mass"
        elif pointMass:
            title += "Point Mass"
        
        
        # ----|Initialize Window|----
        self.window = tk.Toplevel()
        self.window.protocol("WM_DELETE_WINDOW", self.close)
        self.window.title(title)
        self.window.iconbitmap(Solarsim.IMG_WINDOWICON)
        self.window.configure(bg = Solarsim.COLOR_ACCENT)
        
        
        # ----|Plot Results|----
        # Define Colors
        self.PLOT_COLORS = {self.KEYS[i]:self.COLORS[i]
                            for i in range(len(self.COLORS))}
        plot = self.plot(results)
        plot.pack(side = tk.TOP, fill = tk.BOTH, expand = 1)
        
        
        # Start mainloop
        self.window.mainloop()
    
    
    def plot(self, results):
        """
        plot returns a Tkinter Matplotlib widget to be displayed in the Graph
        window.

        INPUT:
        results : dictionary
            2D dictionary containing the positions at each time step for each
            body input by the user. Format is as follows:
                results = {"star": np.zeros(3),
                           "planets": {"planet1": np.array((numsSteps, 3)),
                                       "planet2": np.array((numsSteps, 3)),
                                           .
                                           .
                                           .
                                       "planetN": np.array((numsSteps, 3))},
                           "point": np.array((numsSteps, 3))}

        OUTPUT:
        plot : Tkinter Widget
            Tkinter Matplotlib 3D Plot widget containing the results of the
            simulation.
        """
        
        # ----|Create Figure|----
        # Set Backend
        matplotlib.use("TkAgg")
        
        # Create
        fig = Figure(figsize = (10,8))
        figCanvas = FigureCanvasTkAgg(fig, self.window)
        
        # Toolbar
        NavigationToolbar2Tk(figCanvas, self.window)
        
        # Axes
        ax = fig.add_subplot(projection = "3d")
        
        
        # ----|Plot|----
        maxX, maxY, maxZ = 0,0,0
        
        # Star
        ax.scatter3D(0,0,0, color = "#000000", s = 4.0, marker = "*")
        
        # Planets
        if "planets" in results.keys():
            for planet in results["planets"].keys():
                # Check for new max
                tempX = max(abs(results["planets"][planet][:,0]))
                tempY = max(abs(results["planets"][planet][:,1]))
                tempZ = max(abs(results["planets"][planet][:,2]))
                
                maxX += (tempX - maxX)*(tempX > maxX)
                maxY += (tempY - maxY)*(tempY > maxY)
                maxZ += (tempZ - maxZ)*(tempZ > maxZ)
                
                # Plot Results
                ax.plot3D(results["planets"][planet][:,0],
                          results["planets"][planet][:,1],
                          results["planets"][planet][:,2],
                          color = self.PLOT_COLORS[planet],
                          label = planet)
                
        # Point Mass
        if "point" in results.keys():
            # Check for new max
            tempX = max(abs(results["point"][:,0]))
            tempY = max(abs(results["point"][:,1]))
            tempZ = max(abs(results["point"][:,2]))
            
            maxX += (tempX - maxX)*(tempX > maxX)
            maxY += (tempY - maxY)*(tempY > maxY)
            maxZ += (tempZ - maxZ)*(tempZ > maxZ)
            
            # Plot Results
            ax.plot3D(results["point"][:,0],
                      results["point"][:,1],
                      results["point"][:,2],
                      color = self.PLOT_COLORS["Point Mass"],
                      label = "Point Mass")
        
        # ----|Configure Plot|----
        # Limits
        maxX *= 1.1
        maxY *= 1.1
        maxZ *= 1.1
        
        ax.set_xlim3d(left = -1*maxX, right = maxX)
        ax.set_ylim3d(bottom = -1*maxY, top = maxY)
        ax.set_zlim3d(bottom = -1*maxZ, top = maxZ)
        
        # Labels
        ax.set_xlabel("X - m")
        ax.set_ylabel("Y - m")
        ax.set_zlabel("Z - m")
        ax.set_title("HCI Coordinates")
        ax.legend()
        
        # ----|Create Widget and Return|----
        plot = figCanvas.get_tk_widget()
        
        return plot
    
    
    def close(self):
        """
        Handles the closure of the Graph window.
        """
        
        self.window.quit()
        self.window.destroy()