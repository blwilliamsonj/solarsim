# ----------------------------------------------------------------------------
# Bryan Williamson
# 
# --|run.py|------------------------------------------------------------------
# run.py handles starting up the entire solarsim program.
#-----------------------------------------------------------------------------

# --|Imports|-----------------------------------------------------------------
import sys
import solarsim as ss
sys.path.insert(0, ss.LIBDIR)
import ctypes


# --|Main Method|-------------------------------------------------------------
def startGUI():
    # Set DPI Awareness
    try: # Win 8.1 >
        ctypes.windll.shcore.SetProcessDpiAwareness(2)
    except:
        try: # Win 8.0 <
            ctypes.windll.user32.SetProcessDPIAware()
        except:
            pass
    
    # Create solarsim object
    ss.Solarsim()


if __name__ == "__main__":
    startGUI()